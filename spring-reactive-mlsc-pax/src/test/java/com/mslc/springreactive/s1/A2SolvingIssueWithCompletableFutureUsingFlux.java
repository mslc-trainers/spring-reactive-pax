package com.mslc.springreactive.s1;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;

public class A2SolvingIssueWithCompletableFutureUsingFlux {

	@Test
	public void using_future() throws InterruptedException, ExecutionException {

		
	}

	/**
	 * This test illustrates the difference between Async v/s Sync. The sync methods
	 * will execute in the containing threads where as async will execute in default
	 * or supplied Executor
	 * 
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void understanding_async() throws InterruptedException {
	
	}

	@Test
	public void using_completablefuture() throws InterruptedException, ExecutionException {

	}

	@Test
	public void using_reactive_api_instead_of_completablefuture() throws InterruptedException {

	

	}

	public void fallBackAction() {

	}

	private Integer executeAsynCall(Supplier<Integer> v) {
		System.out.println("Task 2 started");
//		try {
//			Thread.sleep(3000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return 2 + v.get();
	}

	public Supplier<Integer> action2(Integer value) {

		System.out.println("Task 1 started...");
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return () -> 1 * 100;
	}

}
