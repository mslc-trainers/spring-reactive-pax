package com.mslc.springreactive.s1;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

public class A1UsingCompletableFuture {

	@Test
	public void register_handler_to_execute_on_completion_of_task() throws InterruptedException {

		
	}

	@Test
	public void the_whencomplete_pyramid() throws InterruptedException {
		
	

	}
	
	@Test
	public void the_whencomplete() throws InterruptedException {
		
	
		
	}

	@Test
	public void would_result_in_maintenance_hell() throws InterruptedException, ExecutionException {

	

	}

	public void fallBackAction(Throwable t) {

		System.out.println("The task returned an error : " + t.getMessage());

	}

	public Supplier<String> action() {

		System.out.println("action 1 started...");
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return () -> "action 1";
	}

}
