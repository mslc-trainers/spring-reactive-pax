package com.mslc.springreactive.tr.c2.controllers;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@WebFluxTest
public class FluxAndMonoControllerTest {

	

	@Test
	public void playing_with_response() {

		// @formatter:off

		
		   
		   
	}
	
	@Test
	public void when_int_flux_get_ints() {

		// @formatter:off
	
	}

	@Test
	public void when_int_flux_get_ints_using_expectBodyList() {

		// @formatter:off

	}

	@Test
	public void when_int_flux_get_ints_using_expectBodyList_returnResult() {

		List<Integer> expectedValues = Arrays.asList(1, 2, 3, 4, 5, 6);
		// @formatter:off
		
	}

	/**
	 * 
	 * Exercise for you - use consumeWith
	 */
	@Test
	public void when_int_flux_get_ints_using_expectBodyList_consumeWith() {

		List<Integer> expectedValues = Arrays.asList(1, 2, 3, 4, 5, 6);
		// @formatter:off
	

	}

	@Test
	public void when_stream_then_use_cancel() {
		// @formatter:off

		

	}

	@Test
	public void when_mono() {
		// @formatter:off


	}

}
