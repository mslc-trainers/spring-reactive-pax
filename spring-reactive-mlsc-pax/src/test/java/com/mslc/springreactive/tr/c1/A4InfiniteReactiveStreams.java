package com.mslc.springreactive.tr.c1;

import org.junit.jupiter.api.Test;

public class A4InfiniteReactiveStreams {

	@Test
	public void when_infinite_sequence() {

		// @formatter:off
		
		

	}

	/**
	 * Take will take only speicified number of values and then will invoke complete
	 * event
	 * <hR>
	 * Make note of the fact that the 1st test does continue as the thread of test
	 * case is still active
	 * 
	 */
	@Test
	public void when_infinite_sequence_with_take() {

		// @formatter:off

	

	}

	/**
	 * <h2>Assignment</h2> Use a map to convert to Integer Flux and then use
	 * StepVerifier
	 * 
	 */
	@Test
	public void when_infinite_sequence_with_map_take() {

	}

}
