package com.mslc.springreactive.tr.c1;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

/**
 * Just a peek into Flux and Mono and then take a look at the the deck
 * 
 * 
 * @author muhammedshakir
 *
 */
public class A1MoreWithFluxErrorHandlingAndTesting {

	List<String> myClients = Arrays.asList("JPMC", "Nomura", "Morgan Stanley", "BNP Pariba", "BoA");

	/**
	 * 
	 * Create flux with .just and the concat the same with another flux agains
	 * created with Flux.just
	 * 
	 * Using concatWith to concat with Flux.error as well log it and see the results
	 * 
	 * Use subscribe to swing the Flux in action
	 * 
	 * 
	 */
	@Test
	public void flux_contactWith() {

		// @formatter:off
		
	}

	/**
	 * Create flux of strings concatWith Flux.error Log
	 * <ol>
	 * <li>Use StepVerifier in order to test</li>
	 * <li>Use expectNext</li>
	 * <li>Use expectError</li> Use verify()</li>
	 * </ol>
	 */
	@Test
	public void flux_test_element_with_error() {

		

	}

	/**
	 * 
	 * Create flux
	 * <ol>
	 * <li>use StepVerifier</li>
	 * <li>use expectNextCount()</li>
	 * </ol>
	 */
	@Test
	public void flux_test_elements_with_expect_count() {

		// @formatter:off
 

	}

	/**
	 * 
	 * Create flux
	 * <ol>
	 * <li>use StepVerifier</li>
	 * <li>use expectNextCount()</li>
	 * <li>use expectErrorMessage()</li>
	 * </ol>
	 */
	@Test
	public void flux_test_elements_with_expect_error_message() {

		// @formatter:off
		
	}

	/**
	 * 
	 * You can create a Flux or a Mono and directly pass the same to the
	 * StepVerifier
	 */
	@Test
	public void mono_error() {

		// you may want to use .log as well
		// @formatter:off
		

	}

	@Test
	public void when_repeat() {

		

	}

	/**
	 * 
	 * use map to covert string to its respective length
	 * <ol>
	 * <li>use map</li>
	 * <li>use repeat</li>
	 * <li>repeat() will result in infinite repeat</li>
	 * <li>repeat(1) will result in 1 time repeat of subscription</li>
	 * <li>use map</li>
	 * </ol>
	 * 
	 */
	@Test
	public void using_map_on_flux() {
		List<String> myClients = Arrays.asList("JPMC", "Nomura", "Morgan Stanley", "BNP Pariba", "BoA");
		// 4, 6, 14, 10, 3
		// @formatter:off

	

	}

	@Test
	public void when_flatMap_with_streams() {

		Stream<String> names = Stream.of("a", "b", "c");

		

	}

	/**
	 * 
	 * Using flatMap to convert to process list
	 *
	 * 
	 */
	@Test
	public void using_flatmap_on_flux() {

		// @formatter:off

	}

	/**
	 * Step by step
	 * <h2>First understand the concept of window</h2>
	 * <ol>
	 * <li>Create flux of string of 3 strings</li>
	 * <li>use log</li>
	 * <li>use window(1)</li>
	 * <li>note that you will get Flux<Flux<String>></li>
	 * <li>Use a StepVerifier to asser that the Flux<Flux<String>> has 3
	 * elements</li>
	 * <li>Change to window(2) - <b>now you will get 2 elements</li>
	 * </ol>
	 * 
	 * 
	 */
	@Test
	public void using_flatmap_on_flux_parallel() {

		// @formatter:off

	}

	private List<String> concatenateItem(String s) {

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Arrays.asList(s, "1");
	}

	@Test
	public void handle_error_using_on_error_map() {
		// @formatter:off
		
	}

	/**
	 * use onErrorMap to convert the exception. Also use retry Also use expectError
	 * in StepVerifier
	 * <hr>
	 * .retry will retry the processing and then onErrorMap will be executed <b> :
	 * Note that retry works only when error is actuall thrown. The situations where
	 * errors are handled gracefully like (onErrorReturn), it will not work
	 */
	@Test
	public void handle_error_using_on_error_map_with_retry() {
		// @formatter:off
	
	}

	/**
	 * use onErrorMap to convert the exception. Also use retry Also use expectError
	 * in StepVerifier
	 * <hr>
	 * .retryWithBackOff will do a retry after the duration specified
	 */
	@Test
	public void handle_error_using_on_error_map_with_retryBackOff() {
		// @formatter:off
		

	}

}
