package com.mslc.springreactive.tr.c1;

import org.junit.jupiter.api.Test;

public class A2MergingAndConcatingFlux {

	/**
	 * 
	 *  Merge, merges two fluxes and executes the processing in containing thread
	 */
	@Test
	public void when_merge_flux_uses_containing_thread() {

		// @formatter:off
		

	}

	
	/**
	 * 
	 * using delay results into using parrallelism. However, the order is not maintained
	 * 
	 */
	@Test
	public void when_merge_with_delayelements_flux_uses_parallel_threads() {

		// @formatter:off

		
	}

	/**
	 * use concat in order to maintain the order. but this will result in slow processing though it does parallel processing
	 * 
	 */
	@Test
	public void when_concat_with_delayelements_flux_maintains_order_but_slow() {

		// @formatter:off
		
		
	}

	/**
	 * Use delayElements in order to delay the onNext.
	 * <hr>
	 * then use Flux.zip in order concatenate elements from each flux. You will have
	 * to use a BiConsumer to provide the concatenation mechanism
	 * 
	 * 
	 * 
	 * 
	 */
	@Test
	public void when_zip_flux_uses_parallel_threads() {

		// @formatter:off
	

	}

}
