package com.mslc.springreactive.tr.c1;

import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscription;

import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

/**
 * 
 * Take a look at the deck and come back to the test cases here
 * 
 * 
 * @author muhammedshakir
 *
 */
public class A5BackPressureInReactiveStreams {

	/**
	 * 
	 * use thenRequest(1) to request for elements
	 * 
	 * @throws InterruptedException
	 * 
	 */
	@Test
	public void backpressure_on_flux() throws InterruptedException {

		// @formatter:off

	
		

	}

	@Test
	public void backpressure_on_flux_request_subscription() {

		// @formatter:off

		
			
	 }
	
	
	@Test
	public void backpressure_on_flux_cancel_subscription() {
		
		// @formatter:off
		
		
		
	}
	
	
	/**
	 * As rightly mentioned by Dipanjan, ThreadPoolExecutor is not the solution to your use case. At the most one can use ThreadPoolExecutor with a RejectedExecutionHandler and then perhaps resubmit the task.  Your requirement can be fulfilled with the Infrastructure where in the Producer is BackPressure aware. If you have liberty to use Project Reactor then you can do some thing like I have demonstrated in following PoC Code
	 */
	
	@Test
	public void backpressure_on_flux_custom_subscriber() {
		// @formatter:off
		
	}
	 

}
