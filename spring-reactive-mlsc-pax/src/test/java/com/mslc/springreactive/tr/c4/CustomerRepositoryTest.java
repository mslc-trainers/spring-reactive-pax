package com.mslc.springreactive.tr.c4;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@DataMongoTest
@RunWith(SpringRunner.class)
@TestInstance(Lifecycle.PER_CLASS)
public class CustomerRepositoryTest {

	Logger _log = LoggerFactory.getLogger(CustomerRepositoryTest.class);

	// @formatter:off

		static List<Customer> testData = Arrays
				.asList(new Customer("1", "JPMC", "Mumbai"),
						new Customer(null, "Morgan Stanley", "Mumbai"), 
						new Customer(null, "BNP Pariba", "Mumbai"));
	 
	// @formatter:on

	@Autowired
	CustomerRepository customerRepo;

	@BeforeAll
	public void setUp() {

		/**
		 * blockLast : Subscribe to this Flux and block indefinitely until the upstream signals its
		 * last value or completes. 
		 * 
		 */

		// @formatter:off

//		customerRepo
//			.deleteAll()
//			.thenMany(Flux.fromIterable(testData))
//			.flatMap(x -> customerRepo.save(x))
//			.doOnNext(x -> {
//				
//					_log.info(" inserted : " + x.getId() + " -- " + x.getName());
//			}).blockLast();
		
		customerRepo.deleteAll();

		// Way 1 where you use map  
		Flux.fromIterable(testData)
		   .map(x -> {
			   Mono<Customer> newCustomer = customerRepo.save(x);
			   return newCustomer;
		   })
		   .doOnNext(x -> {
			   Customer c = x.block();
			   _log.info("Added : " + c.getId() + " -- " + c.getName());
		   })
		   .blockLast();
//		   .log();
		
		// Way 2
		Flux.fromIterable(testData)
			// flatMap take a function that takes a publisher and returns
			// Object from publisher. .save returns a Mono 
		   .flatMap(x -> customerRepo.save(x))
		   .doOnNext(x -> {
			   _log.info("New Customer : "+ x.getId() + " -- " + x.getName());
		   })
		   .log()
		   .blockLast();

	}

	@Test
	@Order(50)
//	@AfterAll
	public void test_find_all_customers() {

		// @formatter:off


	}

	@Test
	public void test_find_by_id() {

		// @formatter:off

		
	}

	@Test
	@Order(3)
	public void test_find_by_address() {

		// @formatter:off
		

		


	}

	@Test
	@Order(1)
	public void test_create_customer() {

		// @formatter:off

	

	}

	@Test
	@Order(2)
	public void test_update_customer() {

		String newAddress = "New York";

		
	}

	@Test
	@Order(51)
	public void test_delete_customer() {

		// @formatter:off

	

	}

}
