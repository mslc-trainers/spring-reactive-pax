package com.mslc.springreactive.tr.c6;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.mslc.springreactive.tr.c4.Customer;
import com.mslc.springreactive.tr.c4.CustomerRepository;
import com.mslc.springreactive.tr.c5.APIConstants;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureWebTestClient
@TestInstance(Lifecycle.PER_CLASS)
@ActiveProfiles("test")
@TestMethodOrder(OrderAnnotation.class)
public class CustomerAPIHandlerTest {

	@Autowired
	WebTestClient webTestClient;

	@Autowired
	CustomerRepository customerRepo;

	private List<Customer> data() {

		// @formatter:off

		return Arrays.asList(
				new Customer("101", "IBM", "Bangalore"), 
				new Customer("102", "BNP Pariba", "Mumbai"),
				new Customer("103", "Morgan Stanley", "Hyderabad"), 
				new Customer("104", "JPMC", "Hyderabad")
				);
		 
	// @formatter:on

	}

	@BeforeAll
	public void setUp() {
		// @formatter:off

		customerRepo.deleteAll()
		    .thenMany(Flux.fromIterable(data()))
		    .flatMap(x -> {
		    	return customerRepo.save(x);
		    })
		    .doOnNext(x -> {
		    	System.out.println("added :" + x.getId()  + " -- " + x.getName());
		    }).blockLast();
		 
		// @formatter:on

	}

	@Test
	@Order(1)
	public void when_get_customers_then_return_customers() {

		// @formatter:off

	

	}

	@Test
	@Order(2)
	public void when_get_customer_by_customerId_then_return_customer() {

		// @formatter:off
		
		

	}

	@Test
	@Order(3)
	public void when_invalid_get_customer_then_notfound() {

		// @formatter:off
		
	
	}

	@Test
	@Order(4)
	public void when_create_customer_get_new_customer() {

		Customer c = new Customer("505", "Nomura", "Mumbai");
		// @formatter:off
		
	

	}

	@Test
	@Order(5)
	public void when_update_customer() {

		// @formatter:off

	

	}

	@Test
	@Order(6)
	public void when_delete_customer() {

		// @formatter:off

	

	}

}
