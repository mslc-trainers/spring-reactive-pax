package com.mslc.springreactive.tr.c5;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.mslc.springreactive.tr.c4.Customer;
import com.mslc.springreactive.tr.c4.CustomerRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;


@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureWebTestClient
@TestInstance(Lifecycle.PER_CLASS)
@ActiveProfiles("test")
@TestMethodOrder(OrderAnnotation.class) 
public class CustomerRestControllerTest {

	
	/**
	 * 
	 * Repo is bound only to build test data
	 */
	@Autowired
	CustomerRepository customerRepo;

	private List<Customer> data() {

		// @formatter:off

		return Arrays.asList(
				new Customer("101", "IBM", "Bangalore"), 
				new Customer("102", "BNP Pariba", "Mumbai"),
				new Customer("103", "Morgan Stanley", "Hyderabad"), 
				new Customer("104", "JPMC", "Hyderabad")
				);
		 
	}

	@BeforeAll
	public void setUp() {
		// @formatter:off

	
	}

	@Test
	@Order(1)
	public void test_get_customers() {
		
	
			
	}
	
	@Test
	@Order(1)
	public void test_get_customers_using_consume_with() {
		
		
		
	}
	@Test
	@Order(1)
	public void test_get_customers_using_return_result() {
		
		// @formatter:off
	
	}
	
	@Test
	@Order(2)
	public void test_get_customerById_then_return_customer() {
		
		// @formatter:off


		
	}
	@Test
	@Order(2)
	public void test_get_customerById_with_response_entity_then_return_customer() {
		
		// @formatter:off
		
		
		
	}
	
	@Test
	public void test_get_customerById_not_found() {
		
		// @formatter:off
		
	
		
		
	}
	
	@Test
	public void test_create_new_customer() {
		
		// @formatter:off
		
		Customer customer = new Customer("555", "Bank of America", "Chennai" );
		
	
		
		
	}
	
	@Test
	@Order(99)
	public void test_delete_customer() {
		
		// @formatter:off
		
		
		
		
	}
	
	@Test
	@Order(45)
	public void test_update_customer() {
		
		// @formatter:off
		
		Customer customer = new Customer("101", "IBM India", "Delhi");
		
		
		
	}
	@Test
	@Order(45)
	public void test_update_invalid_customer() {
		
		// @formatter:off
		
		Customer customer = new Customer("101", "IBM India", "Delhi");
		
	
		
		
	}

}
