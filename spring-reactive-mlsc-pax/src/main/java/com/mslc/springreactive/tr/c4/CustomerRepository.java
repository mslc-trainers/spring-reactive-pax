package com.mslc.springreactive.tr.c4;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import reactor.core.publisher.Flux;


@Repository
public interface CustomerRepository extends ReactiveMongoRepository<Customer, String> {
	
	
	

}
	