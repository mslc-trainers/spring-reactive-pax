package com.mslc.springreactive.tr.c2.controllers;

import java.time.Duration;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class FluxAndMonoController {

	@GetMapping(path = "/int-flux", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Flux<Integer> handleIntFluxRequest() {

		return null;

	}

	@GetMapping(path = "/int-flux-stream", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
	public Flux<Long> handleIntFluxStreamRequest() {

		return null;

	}

	@GetMapping(path = "int-mono")
	public Mono<Integer> handleIntMonoRequest() {

		return null;

	}

}
