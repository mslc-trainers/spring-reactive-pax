package com.mslc.springreactive.tr.c5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.mslc.springreactive.tr.c4.Customer;
import com.mslc.springreactive.tr.c4.CustomerRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class CustomerRestController {

	@GetMapping(path = APIConstants.GET_CUSTOMERS, produces = { MediaType.APPLICATION_JSON_VALUE })
	public Flux<Customer> handleGetCustomers() {

		// @formatter:off

		return null;

	}

	@GetMapping(path = APIConstants.GET_CUSTOMERS + "/{customerId}")
	public Mono<Customer> handleGetCustomerById(@PathVariable(name = "customerId") String customerId) {

		// @formatter:off

		return null;

	}

	@GetMapping(path = APIConstants.GET_CUSTOMERS + "/with-response-entity/{customerId}", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public Mono<ResponseEntity<Customer>> handleGetCustomerByIdWithResponseEntity(
			@PathVariable(name = "customerId") String customerId) {

		// @formatter:off

		return null;

	}

	@PostMapping(path = APIConstants.GET_CUSTOMERS, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public Mono<Customer> handlePostCustomer(@RequestBody Customer customer) {

		// @formatter:off
		

		return null;


	}

	@DeleteMapping(path = APIConstants.GET_CUSTOMERS + "/{customerId}")
	public Mono<Void> handleDeleteCustomer(@PathVariable(name = "customerId") String customerId) {

		// @formatter:off
		

		return null;

	}

	@PutMapping(path = APIConstants.GET_CUSTOMERS + "/{customerId}")
	public Mono<ResponseEntity<Customer>> handlePutCustomer(@PathVariable String customerId,
			@RequestBody Customer customer) {

		// @formatter:off
		

		return null;


	}

}
