package com.mslc.springreactive.tr.c6;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.mslc.springreactive.tr.c4.Customer;
import com.mslc.springreactive.tr.c4.CustomerRepository;

import reactor.core.publisher.Mono;

public class CustomerAPIHandler {

	@Autowired
	CustomerRepository customerRepo;

	public Mono<ServerResponse> getAllCustomers(ServerRequest serverRequest) {

		// @formatter:off

		return null;

	}

	public Mono<ServerResponse> getCustomer(ServerRequest serverRequest) {

		// @formatter:off

		return null;

	}

	public Mono<ServerResponse> createCustomer(ServerRequest serverRequest) {

		// @formatter:off

		return null;

	}

	public Mono<ServerResponse> deleteCustomer(ServerRequest serverRequest) {
		// @formatter:off

		return null;

	}

	public Mono<ServerResponse> updateCustomer(ServerRequest serverRequest) {

		// @formatter:off

		return null;

	}

}
