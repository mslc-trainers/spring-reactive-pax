package com.mslc.springreactive.tr.c5;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.mslc.springreactive.tr.c4.Customer;
import com.mslc.springreactive.tr.c4.CustomerRepository;

import reactor.core.publisher.Flux;

@Component
@Profile("!test")
public class CustomerDataInitializer implements CommandLineRunner {

	public CustomerDataInitializer() {

	}

	@Autowired
	CustomerRepository customerRepo;

	@Override
	public void run(String... args) throws Exception {

		initializeData();

	}

	private List<Customer> data() {

		// @formatter:off
		return Arrays.asList(
				new Customer("101", "IBM", "Bangalore"),
				new Customer("102", "BNP Pariba", "Mumbai"),
				new Customer("103", "Morgan Stanley", "Hyderabad"),
				new Customer("104", "JPMC", "Hyderabad")
				
				);
		
		 
		// @formatter:on

	}

	private void initializeData() {

		// @formatter:off

		// the code to intialize the data goes here

	}

}
