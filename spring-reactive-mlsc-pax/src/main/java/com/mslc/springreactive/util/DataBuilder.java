package com.mslc.springreactive.util;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import com.mslc.springreactive.model.Invoice;
import com.mslc.springreactive.model.Product;

public class DataBuilder {

	public static List<Invoice> buildInvoices() {

		List<Invoice> invoices = Arrays.asList(
				new Invoice(101, LocalDate.now(),
						Arrays.asList(new Product(101, "Mouse"), new Product(102, "Keyboard"))),
				new Invoice(101, LocalDate.now(), Arrays.asList(new Product(105, "laptop"))));

		return invoices;

	}

}
