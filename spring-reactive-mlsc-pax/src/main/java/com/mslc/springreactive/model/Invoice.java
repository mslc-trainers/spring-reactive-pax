package com.mslc.springreactive.model;

import java.time.LocalDate;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Invoice {
	
	private int invoiceId;
	private LocalDate invoiceDate;
	private List<Product> products;

}
