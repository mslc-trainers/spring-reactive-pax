package com.example.demo;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class MoreOnParallelismInSpringBootReactive {

	@Test
	public void explainining_parallelism() { 
		
		
		Flux<Integer> f = Flux.range(0, 10);
		
		// Part 1
		// .runOn : filter, map, flatMap : 
//		
//		f.parallel()
//		 .runOn(Schedulers.newParallel("my-thread-pool", 16))
//		 .map(x -> {
//			 
//			 try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			 System.out.println("Thread : " + Thread.currentThread().getName());
//			 return x;
//		 })
//		 .sequential()
//		 .log()
//		 .blockLast();
		
		
		// Part 2
		
		// .subscribeOn
		// .publishOn
		
		
//		f
//		   .map(x -> x)
//		   .filter(x -> true)
//		  
//		   .map( x -> x)
//		   .subscribe(Schedulers.parallel());
//		   
		
		
		
		
		
		
		
		
		
		
	}

}
