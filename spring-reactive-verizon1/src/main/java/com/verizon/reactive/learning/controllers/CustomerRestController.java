package com.verizon.reactive.learning.controllers;

import javax.annotation.PostConstruct;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.verizon.reactive.learning.model.Customer;
import com.verizon.reactive.learning.model.CustomerRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class CustomerRestController {

	@Autowired
	CustomerRepository customerRepo;

	@PostMapping (path ="/customers")
	public Mono<ResponseEntity<Customer>> handlePostCusotmer(@RequestBody Customer customer) {
		
		System.out.println("received : " +
						customer.getAddress() + " -- " + 
				customer.getId() + " -- " + customer.getName());
		
		// please make use customerRepo.
		
		Mono<Customer> m =  customerRepo.save(customer);
		
		Mono<ResponseEntity<Customer>> r =   
				m.map(x -> new ResponseEntity<>(x, HttpStatus.OK));
		
		
		return r;
	}

	@GetMapping(path = "/customers/{customerId}")
	public Mono<ResponseEntity<Customer>> handleGetCustomer(@PathVariable String customerId) {

		Mono<Customer> monoCustomer = customerRepo.findById(customerId);

		Mono<ResponseEntity<Customer>> response = monoCustomer.map(x -> new ResponseEntity<>(x, HttpStatus.OK))
				.defaultIfEmpty(new ResponseEntity<>(HttpStatus.NO_CONTENT));

//		ResponseEntity<Mono<Customer>> response = 
//				  new ResponseEntity<Mono<Customer>>(monoCustomer, HttpStatus.OK);

		return response;

	}

	@GetMapping(path = "/customers", produces = { MediaType.APPLICATION_STREAM_JSON_VALUE })
	public ResponseEntity<Flux<Customer>> handleGetCustomers() {

		System.out.println("Request received : " + Thread.currentThread().getName());

//		List<Customer> customers = DataHelper.getCustomers();
		Flux<Customer> customersFlux = customerRepo.findAll();

//		Flux<Customer> customersFlux = Flux.fromIterable(customers);

		// @formatter:off

		Flux<Customer> f2 = 
				 customersFlux
				 .map(x -> {
					 
//					 try {
////						Thread.sleep(2000);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
					 System.out.println(x.getName() + " is getting processed in "  + Thread.currentThread().getName());
					 Customer c = new Customer();
					 BeanUtils.copyProperties(x, c);
					 c.setName(x.getName().toUpperCase());
					 
					 return c;
					 
				 });
		    
		

		ResponseEntity<Flux<Customer>> response = new ResponseEntity<>(f2, HttpStatus.OK);

//		response = new ResponseEntity(null, HttpStatus.NO_CONTENT);
		System.out.println("Request completed : " + Thread.currentThread().getName());
		System.out.println(" ------ ");

		return response;

	}

}
