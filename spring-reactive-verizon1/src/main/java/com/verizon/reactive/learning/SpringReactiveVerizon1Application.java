package com.verizon.reactive.learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringReactiveVerizon1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringReactiveVerizon1Application.class, args);
	}

}
