package com.verizon.reactive.learning.model;

import java.util.Arrays;
import java.util.List;

public class DataHelper {

	
	public static List<Customer> getCustomers() {
		
		
		return Arrays.asList(new Customer("101", "IBM", "Mumbai"),
				new Customer("102", "Nomura", "bangalore"),
				new Customer("103", "Verizon", "hyd"));
		
		
	}
}
