package com.example.demo.controllers;

import java.time.Duration;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

@RestController
public class DemController {
	
	
	/**
	 * 
	 * Emitting
	 * 
	 * @return
	 */
	
	@GetMapping (path = "/demo",  produces = {MediaType.APPLICATION_STREAM_JSON_VALUE}) 
	public Flux<Long> handleGetDemo() {
		
		Scheduler s = Schedulers.parallel();
		
		
		System.out.println("1 >> Request received in " + Thread.currentThread().getName());
		
		Flux<Long> f1 = 
				  Flux.interval(Duration.ofSeconds(1))
				      .map(x -> x + 1)
				      .log()
				      .take(10);
		
		
		System.out.println("2 >> Request received in " + Thread.currentThread().getName());
		
		return f1;
		
		
	}

}
