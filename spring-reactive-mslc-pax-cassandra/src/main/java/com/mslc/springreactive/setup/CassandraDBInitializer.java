package com.mslc.springreactive.setup;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.mslc.springreactive.model.Customer;
import com.mslc.springreactive.model.CustomerRepository;

import reactor.core.publisher.Flux;

@Component
public class CassandraDBInitializer implements CommandLineRunner {

	@Autowired
	CustomerRepository customerRepo;
	
	@Override
	public void run(String... args) throws Exception {
		
		initializeData();
		
	}


	private List<Customer> data() {
		
		return Arrays.asList(
				new Customer("101", "IBM", "Bangalore"),
				new Customer("102", "BNP Pariba", "Mumbai"),
				new Customer("103", "Morgan Stanley", "Hyderabad"),
				new Customer("104", "JPMC", "Hyderabad")
				
				);
		
		
	}
	
	private void initializeData() {
		
		customerRepo.deleteAll()
				.thenMany(Flux.fromIterable(data()))
				.flatMap(x -> {
					
					return customerRepo.save(x);
				})
				.thenMany(customerRepo.findAll())
				.subscribe(x -> {
					System.out.println("initialized : " + x.getId() + " -- " + x.getName());
				});
		
		
	}

}
