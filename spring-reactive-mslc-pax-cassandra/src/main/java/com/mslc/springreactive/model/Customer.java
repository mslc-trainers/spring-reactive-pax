package com.mslc.springreactive.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Table(value = "customer",  forceQuote = true)
@NoArgsConstructor
@AllArgsConstructor
public class Customer {
	
	@Id
	private String id;
	private String name;
	private String address;
	
	

}
