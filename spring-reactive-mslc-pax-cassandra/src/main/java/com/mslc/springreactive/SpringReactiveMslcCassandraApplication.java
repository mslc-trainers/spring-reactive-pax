package com.mslc.springreactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

@SpringBootApplication
@EnableCassandraRepositories
//@Import(RepositoryRestMvcConfiguration.class)
@EnableAutoConfiguration

public class SpringReactiveMslcCassandraApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringReactiveMslcCassandraApplication.class, args);
	}

}
