package com.verizon.reactive.learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringReactiveVerizon2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringReactiveVerizon2Application.class, args);
	}

}
