package com.verizon.reactive.learning.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.verizon.reactive.learning.valueobjects.Customer;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class InvoiceController {
	
	
	WebClient webClient = WebClient.create("http://localhost:8080");
	
	@GetMapping (path = "/invoices")
	public String handleGetAllInvoices() {
		
		
		// @formatter:off
	 Flux<Customer> customers =	
			 
			 webClient.get()
				.uri("/customers")
				.retrieve()
				.bodyToFlux(Customer.class);
		
	    customers.subscribe(System.out::println);

	    
//	    webClient
//	      .post()
//	      .uri("/customer")
//	      .bodyValue(new Customer())
//	      .exchange();
//	    
	      
	    
		// @formatter:on

	    
		
		
		return "all-invoices";
	}
	
	
	
	

}
