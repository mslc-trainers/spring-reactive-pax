package demo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ThreadStateDemo {

	public static void main(String[] args) throws IOException, InterruptedException {
		
		MyBoundedStructure ds = new MyBoundedStructure(3);

		ds.put("v1");
		ds.put("v2");
		ds.put("v3");
		MyInteger i = new MyInteger();
		

		Thread t1 = new Thread() {
			public void run() {

				ds.put("v4");
//				i.setValue(50);
				
			};
		};

		Thread t2 = new Thread() {

			public void run() {
				
//				System.out.println("Getting the value : " + i.getValue());
				
				try {
					Thread.sleep(7000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				String value = ds.take();
				System.out.println(value);
				
				
			};
		};

		t1.start();
		
		Thread.sleep(1000);
		
		t2.start();

		new Thread() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(
							"state of t1 is : " + t1.getState().name() + " t2 state : " + t2.getState().name());
				}
			};
		}.start();

	}

}


class MyBoundedStructure {
	
	
	private int size;
	private List<String> values = new ArrayList<>();
	
	public MyBoundedStructure(int capacity) {
		
	
		this.size = capacity;
				
	
	}
	
	public  synchronized static void doSomething() {
		
		
	}
	
	private Object lock1 = new Object();
	
	public  void put(String value) {
		
		/**
		 * 
		 * 
		 */
		
		synchronized (lock1) {
			///
			
		}
		
		
		if (values.size() == 3) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		values.add(value);
		
		
	}
	
	public synchronized String take() {
		
		String value = values.remove(0);
		notify();
		return value;
		
	}
	
	
	
}




class MyInteger {

	private int value;

	public synchronized  void setValue(int value) {
		
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.value = value;

	}

	public synchronized int getValue() {
		return this.value;
	}

}