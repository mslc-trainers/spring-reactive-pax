package demo;

public class Demo {

	public static void main(String[] args) {

		new Thread() {
			{
				setDaemon(true);
			}

			@Override
			public void run() {

				try {
					Thread.sleep(7000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("In thread : : " + Thread.currentThread().getName() + " : "
						+ Thread.currentThread().isDaemon());

			}
		}.start();

		System.out.println(
				"Thread name : " + Thread.currentThread().getName() + " -- " + Thread.currentThread().isDaemon());

	}

}
