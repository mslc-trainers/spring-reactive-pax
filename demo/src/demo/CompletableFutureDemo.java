package demo;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class CompletableFutureDemo {

	
	// GetMapping ( path ="/customers")
	public static void main(String[] args) throws InterruptedException, ExecutionException {

		CompletableFuture<String> stage1 =

				CompletableFuture.supplyAsync(() -> {

					try {
						Thread.sleep(7000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
//					    System.out.println(Thread.currentThread().getName() + " " + Thread.currentThread().isDaemon());

					// external service 1 is called
					return "values returned from service 1";

				});

//		   String value = cf.get();
//		   System.out.println("After getting value : " + value);

		CompletableFuture<String> stage2 = stage1.whenComplete((x, y) -> {

			if (y == null) {
				System.out.println("The value returned back is :" + x);
			}

			// x : external service 2

		});

		CompletableFuture<String> stage3 = stage2.whenComplete((x, y) -> {

			//

			if (y == null) {
				System.out.println(x);
			}

		});

		CompletableFuture<String> s4 = stage3.handle((x, y) -> {

			if (y == null) {
				System.out.println(x + " -- ");
			}

			return "some value";

		});

		System.out.println("After registering the handler");
		
		

		stage1.join();

	}

}
