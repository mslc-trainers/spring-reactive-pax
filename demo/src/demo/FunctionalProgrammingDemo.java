package demo;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntBinaryOperator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public class FunctionalProgrammingDemo {
	
	
	
	public static void main(String[] args) {
		
		TaxCalculator t = x -> x.equalsIgnoreCase("GST") ? 18 : 12;
		
//		manageTaxCalculation(t);
//		
//		manageTaxCalculation(x -> x.equalsIgnoreCase("ST") ? 10 : 15);
//		manageTaxCalculation(x -> x.equalsIgnoreCase("ST") ? 10 : 15);
		
		
		Function<String, Integer> f1 = x -> 5 + 5;
		
		BiFunction<String, String, Integer> f2 = (x, y) -> 5;
		Predicate<String> f3 = x -> x.equals("GST");
		
		BiPredicate<String, String> f4 = (x, y) -> x.equals("GST") && y.equals("MAHA");
		
		
		Consumer<String> f5 = x -> {System.out.println(x);};
		
		/**
		 * 1) static method reference
		 * 2) object method reference
		 * 3) arbitrary object method reference
		 * 4) constructor reference
		 */
		Consumer<String> ff = x -> System.out.println(x);
		Consumer<String> ff1 = System.out::println;
		
		ff.accept("Shakir");
		
		Supplier<Integer> f6 = () -> 12;
		
		BiConsumer<String, String> f7 = (x, y) -> {};
		
		UnaryOperator<String> f8 = x -> x + " new string";
		
		BinaryOperator<String> f9 = (x, y) -> x + y;
		IntBinaryOperator f10 = (x, y) -> x + y;
//		int value = t.calculateTax("GST");
//		
//		System.out.println(value);
		
		
		Supplier<Customer> s1 = () -> new Customer();
		
		Supplier<Customer> s2 = Customer::new;
		
		Customer c = s2.get();
				
		
	}
	

	// high order function
	public static void manageTaxCalculation(TaxCalculator t) {
		
		/**
		 * 
		 * 
		 * 
		 * 
		 */

	}

}

class Customer {
	
	
}

@FunctionalInterface
interface TaxCalculator {

	public int calculateTax(String type);
	
	public default int calculateTaxGST(String type) {
		
		return 10;
	}
	
	public static void printAllTaxes() {
		//
	}
	
	
	

}
