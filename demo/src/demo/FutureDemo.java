package demo;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FutureDemo {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		
		
		
		ExecutorService service = Executors.newCachedThreadPool();
		
		Future<String> futureObject =  service.submit(new Callable<String>() {

			@Override
			public String call() throws Exception {
				
				// external service 1
				
				Thread.sleep(7000);
				
				System.out.println("in task : "  + Thread.currentThread().getName());
				
				return "values1";
				
			}
		});
		
		System.out.println("before get....");
		// this is blocking call.....
		String valueFromService1 = futureObject.get();
		
		System.out.println("Value returned from service1 : " + valueFromService1);

		/**
		 * 
		 * external service 1 returns some values1;
		 * 
		 */

		/**
		 * 
		 * external service 2 returns some values2
		 * 
		 */
		
		
		service.shutdown();

	}

}
