package demo.model;

import java.util.List;

public class Customer {

	private int id;
	private String name;
	private List<CustomerAddress> addresses;

	public Customer() {
		// TODO Auto-generated constructor stub
	}

	public Customer(int id, String name, List<CustomerAddress> addresses) {

		this.id = id;
		this.name = name;
		this.addresses = addresses;
		// TODO Auto-generated constructor stub
	}

	public int getId() {

		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CustomerAddress> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<CustomerAddress> addresses) {
		this.addresses = addresses;
	}

}
