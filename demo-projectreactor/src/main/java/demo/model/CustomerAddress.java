package demo.model;

public class CustomerAddress {

	private int addressId;
	private String city;
	private String state;
	
	public CustomerAddress() {
		// TODO Auto-generated constructor stub
	}
	
	public CustomerAddress(int id, String city) {
		
		this.addressId = id;
		this.city = city;
		
	}
	
	

	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@Override
	public String toString() {
		
		return this.addressId + " -- " + this.city + " -- " + this.state;
	}

}
