package demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import reactor.core.publisher.Flux;

public class Demo {

	public static void main(String[] args) throws InterruptedException {

		// @formatter:off

//		Flux<Long> f1 = 
//				Flux
//				  .interval(Duration.ofSeconds(1))
//				  .log();
		
		
		Flux<Integer> f1 = 
				 Flux.just(1, 2, 3, 4, 5, 6, 7)
				 .log();
		
		List<String> names = Arrays.asList("Morgan", "JP Morgan", "Verizon", "JPMC", "BNP Pariba");
//		names.add("name1");
//		names.add("name2");
		
		Flux<String> f2 = 
				 Flux.fromIterable(names)
				 .log();
		
		f2.subscribe();
		
		 
		// @formatter:on

		f1.subscribe();

//		f1.subscribe(x -> {
//			System.out.println(x +  " -> " + Thread.currentThread().getName());
//		});

		System.out.println("main completed..");

//		Thread.sleep(Integer.MAX_VALUE);

	}
}
