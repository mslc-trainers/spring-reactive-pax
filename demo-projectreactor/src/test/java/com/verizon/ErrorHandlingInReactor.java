package com.verizon;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;

public class ErrorHandlingInReactor {

	@Test
	public void handle_error_using_on_resume() {
		// @formatter:off
		Flux<String> stringFlux = 
				Flux.just("a", "b", "c")
				.concatWith(Flux.error(new RuntimeException("Runtime exception....")))
				.concatWith(Flux.just("Goldman Sachs"))
				.onErrorResume(x -> {
//					_log.info("The error is : " + x.getMessage());
					System.out.println(x.getMessage());
					
					return Flux.just("default value");
				}).log();
		
		
		stringFlux.subscribe();
		
	}
	@Test
	public void handle_error_using_on_return() {
		// @formatter:off
		Flux<Integer> stringFlux = 
				Flux.just(1, 2, 3)
				.concatWith(Flux.error(new RuntimeException("Runtime exception....")))
				.concatWith(Flux.just(5))
				.onErrorReturn(10)
				.log();
		
		
		stringFlux.subscribe();
		
	}
	
	@Test
	public void handle_error_continue() {
		// @formatter:off
		Flux<String> f = 
				 Flux.range(0, 5)
				 .map(x -> {
					 
//					 System.out.println("processing....");
					 if (x == 2) {
						 
						 throw new RuntimeException("runtime exception encountered");
					 }
					 return x + " -- " + 2;
				 })
				 .onErrorContinue((x, y) -> {
					
					 System.out.println("error : " + x.getMessage() + " -- " + y);
				 });
		
		
		f.subscribe(System.out::println);
		
	}
	

}
