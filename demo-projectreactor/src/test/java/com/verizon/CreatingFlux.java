package com.verizon;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscription;

import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class CreatingFlux {

	@Test
	public void back_pressure_demo() {

		List<String> clients = Arrays.asList("BNP Pariba", "BoA", "JP Morgan", "Morgan Stanley", "Nomura");

		Flux<String> fluxOfClients = Flux.fromIterable(clients).log();

		fluxOfClients.subscribe();

	}

	@Test
	public void backpressure_on_flux_request_subscription() {

		// @formatter:off

			Flux<Integer> intFlux = 
					  Flux
					    .range(0, 15)
					    .log();
			
			
			intFlux.subscribe(x -> {},
					
					System.out::println,
					
					() -> {
						// this will executed only when all elements are requested
						System.out.println("complete");
					},
					
					subscription -> subscription.request(10));
			
			
	 }
	
	
	
	@Test
	public void backpressure_on_flux_custom_subscriber() {
		// @formatter:off
		Flux<Integer> intFlux = Flux.range(0, 15).log();
		intFlux.subscribe(new BaseSubscriber<Integer>() {
			
			protected void hookOnComplete() {
				System.out.println("Completed.........");
			}
			
			protected void hookOnSubscribe(Subscription subscription) {
				subscription.request(6);
			}
			
			protected void hookOnNext(Integer value) {
				// 
				if (value > 0 && value % 5 == 0) {
					System.out.println("Requesting next 5 ...");
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					super.request(5);
					
				}
			}
		});
	}
	
	

	@Test
	public void creatingFlux() {

		Mono<Integer> m1 = Mono.just(1);
		m1.subscribe(x -> {
//			System.out.println(x);
		});

		Flux<Integer> f1 = Flux.just(1, 2, 3);

		Flux.fromIterable(Arrays.asList(1, 2, 3, 4));

		Flux<Integer> f3 = Flux.fromArray(new Integer[] { 1, 2, 3 });
		Flux<Integer> f4 = Flux.fromStream(Stream.of(1, 2, 4));
		Flux<Integer> f5 = Flux.from(m1);
		Flux<Integer> f6 = Flux.range(0, 5);

		// @formatter:off

		f1.subscribe(
				
					x -> {System.out.println("received : " + x);}, 
					
					x -> {System.out.println(x.getMessage());},
					
					() -> {
						System.out.println("onComplete.....");
					}
		);
		
		 
		// @formatter:on

	}

	@Test
	public void streams_demo() {

		List<String> clients = Arrays.asList("BNP Pariba", "BoA", "JP Morgan", "Morgan Stanley", "Nomura");
		// @formatter:off

		long start = System.currentTimeMillis();
		List<String> filteredClients = 
				clients
					   .stream()
					   .map(x -> {
						   try {
							Thread.sleep(1000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						   System.out.println("x is getting processed in : " + Thread.currentThread().getName());
						   return x.toUpperCase();
					   })
					   .collect(Collectors.toList());
		long end = System.currentTimeMillis();
		System.out.println(filteredClients + " -- " + (end - start));
					   
		   
		// @formatter:on

	}

	@Test
	public void concatenate_flux() {

		int min = 0;
		int max = 10;

		Flux<Integer> evenNumbers = Flux.range(min, max).filter(x -> x % 2 == 0);

		Flux<Integer> oddNumbers = Flux.range(0, 10).filter(x -> x % 2 > 0);

		Flux<Integer> allInts = Flux.concat(evenNumbers, oddNumbers);

		StepVerifier.create(allInts).expectSubscription().expectNextCount(10)

				.verifyComplete();

//		Mono<List<Integer>> allIntsMono
//		     = allInts
//		         .collectList();
//		        
//		List<Integer> list = allIntsMono.block();

//		List<Integer> allIntsList = allInts.collectList().block();
//		System.out.println(allIntsList);

//		allInts.subscribe(x -> {
//			System.out.println(x);
//		});

	}

}
