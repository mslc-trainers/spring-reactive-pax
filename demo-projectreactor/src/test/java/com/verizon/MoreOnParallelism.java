package com.verizon;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import demo.model.Customer;
import demo.model.CustomerAddress;
import reactor.core.publisher.Flux;
import reactor.core.publisher.ParallelFlux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

public class MoreOnParallelism {

	// @formatter:off

	
	private List<Customer> customers = 
			Arrays.asList(new Customer(101, "BNP Paribas", 
											Arrays.asList(new CustomerAddress(1, "Mumbai"), 
														  new CustomerAddress(1, "Chennai"))),
						   new Customer(101, "Verizon", 
								   			Arrays.asList(new CustomerAddress(1, "Hyd"), 
								   						new CustomerAddress(1, "Bangalore")))
							);
			
			   
			 
	// @formatter:on
	
	
	
	@Test
	public void subscribe_on_scheduler() throws InterruptedException {
		
		List<String> clients = 
				Arrays.asList("BNP Pariba", "BoA",
								"JP Morgan", 
								"Morgan Stanley", "Nomura");
		
		
		
	  
		     
		Flux<String> fluxOfClients = 
				   Flux
				     .fromIterable(clients)
				     .parallel()
				     .runOn(Schedulers.parallel())
				     .filter(x -> {
				    	 
				    	 try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				    	 
				    	 System.out.println(x + " -- filter " + Thread.currentThread().getName());
				    	 return true;
				     })
				     .map(x -> x.toLowerCase())
				     .sequential();
		
		
		
		fluxOfClients.subscribe(x -> {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(" Subscribe  " +  x + " " +  Thread.currentThread().getName());
		});
		
		System.out.println("after subscription.....");
		
		Thread.sleep(10000);
		
		
	}
	
	

	@Test
	public void understanding_flatMap_in_Streams() {

		Stream<Customer> sCustomers = customers.stream();

		// @formatter:off
		
		List<String> namesOfMyCustomers =  
				sCustomers
				  .map(x -> x.getName().toUpperCase())
				  .collect(Collectors.toList());
		
		namesOfMyCustomers.forEach(System.out::println);
		
		
		List<CustomerAddress> addressesOfMyCustomers =  
				customers
				.stream()
				.flatMap(x -> x.getAddresses().stream())
				.collect(Collectors.toList());
		
		addressesOfMyCustomers.forEach(System.out::println);
		
		
		System.out.println(" --------------- ");
		Flux<Customer> fCustomers = Flux.fromIterable(customers);
		
		
		Flux<String> nameOfMyCustomersInFlux =
				   fCustomers
				     .map(x -> x.getName());
		
		nameOfMyCustomersInFlux.subscribe(System.out::println);
		
		Flux<CustomerAddress> fluxOfAddresses =
				   fCustomers
				     .flatMap(x -> Flux.fromIterable(x.getAddresses()));
				
				
		fluxOfAddresses.subscribe(System.out::println);
				 
	    // @formatter:on

	}

	@Test
	public void flux_buffer() throws InterruptedException {

//		  Flux<Integer> f = 
//				  Flux.range(0, 10)
//				  	.buffer(2)
//				  	.flatMap(x -> Flux.fromIterable(x));
//				  	
//		  f.subscribe(x -> {
//			  System.out.println(x);
//		  });

		// @formatter:off

		Flux<Integer> f0 = 
				Flux
				 .range(0, 10)
				 .buffer(1)
				 .flatMap(x -> Flux
						 		.fromIterable(x)
						 		.map(y -> y + 2));

		 
		// @formatter:on

		f0.subscribe(System.out::println);

	}

	@Test
	public void parallelism_with_flux() throws InterruptedException {

		long start = System.currentTimeMillis();
		
		// @formatter:off

		// external 

		Flux<Integer> allInts2 = 
				
				Flux.range(0, 16)
					.buffer(1)
					.flatMap(x -> {

							return Flux
									 .fromIterable(x)
									 .subscribeOn(Schedulers.parallel())
									 .map(y -> {
				
										 
										 System.out.println("Transformation happening of " + y + " in thread : " + Thread.currentThread().getName());
										   // external 
											try {
												Thread.sleep(1000);
											} catch (InterruptedException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
											return y + 1;
									 });

					});
					

		// @formatter:on

		allInts2.subscribe(x -> {
			System.out.println(x + " received :" + Thread.currentThread().getName());
		});
		long end = System.currentTimeMillis();
		System.out.println("time taken : " + (end - start));
		
		
		Thread.sleep(12000);
	}

}
