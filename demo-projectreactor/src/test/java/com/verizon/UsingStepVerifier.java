package com.verizon;

import java.util.Arrays;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class UsingStepVerifier {

	@Test
	public void simple_step_verifier() {
		
		

		// @formatter:off
		
		Flux<String> clients = Flux.fromIterable(Arrays.asList("BNP Pariba", "BoA", "JP Morgan")); //, "Morgan Stanley", "Nomura"));

		StepVerifier
		    .create(clients)
		    .expectSubscription()
		    .expectNext("BNP Pariba")
		    .expectNext("BoA")
		    .verifyComplete();
		 
	  // @formatter:on



	}

}
