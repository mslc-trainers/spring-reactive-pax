package com.verizon;

import java.time.Duration;
import java.util.List;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

public class FluxMergeExample {

//	@Test
	public void understanding_tuples() {
		List<String> l = null;
		Tuple2<Long, Long> tDemo1 = Tuples.of(45L, 20L);

		Tuple2<Float, Long> t2 = tDemo1.mapT1(x -> Float.valueOf(x));

		System.out.println(tDemo1.get(0) + " -- " + tDemo1.get(1) + " -- " + tDemo1.getT1() + " -- " + tDemo1.getT2());
	}

	@Test
	public void understanding_zip() throws InterruptedException {

		Flux<String> f0 = Flux.just("f1-1", "f1-2", "f1-3").log();

		Flux<Long> f1 = Flux.interval(Duration.ofSeconds(1)).log();

//
//		Flux<Tuple2<Long, String>> f3 = f1.zipWith(f0).log();
//		
//		f3.subscribe(x -> {
//			
//			// x ?
//			//System.out.println(x.getT1() + " -- " + x.getT2());
//			
//			
//		});
//		

		Flux<String> f3 = f0.zipWith(f1, (x, y) -> x + " -- " + y);

		f3.subscribe(x -> {

			// x ?
			System.out.println(x);

		});

//		
//		f0.subscribe();
//		f1.subscribe();
//
//		
		Thread.sleep(10000);

	}

	@Test
	public void understanding_merge() throws InterruptedException {

		Flux<String> f0 = Flux.just("f1-1", "f1-2", "f1-3");

		
		Scheduler scheduler = Schedulers.parallel();
		
		Flux<String> f1 = Flux.interval(Duration.ofSeconds(1))
							.zipWith(f0, (x, y) -> y).log();

		// @formatter:off

		Flux<String> f2 = Flux.just("f2-1", "f2-2")
				.doOnNext(x -> {
			
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			   }).log();	 
		// @formatter:on

		Flux<String> f3 = Flux.just("f3").log();
		
//		Flux.merge(f1, f2, f3)
//		    .log()
//		    .subscribe();
//
		
		Flux.concat(f1, f2, f3)
		     .log()
		     .subscribe();
		
		
		Thread.sleep(10000);
		
		
		

	}

	/**
	 * 
	 * concat zip merge
	 * 
	 * 
	 * @throws InterruptedException
	 */
//	@Test
	public void understanding_merge1() throws InterruptedException {

		// @formatter:off
		Flux<String> f0 = Flux.just("f1-1", "f1-2", "f1-3");
		
		
		

		Flux<String> f1  = 
				 Flux
				    .interval(Duration.ofSeconds(1))
				    
				    
				    .zipWith(f0, (x, y) -> y);
		
		Flux<String> f2 = Flux.just("f2-1", "f2-2")
						.doOnNext(x -> {
						
							try {
								Thread.sleep(2000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						});
		Flux<String> f3 = Flux.just("f3");
		
		// the doc says that subscriber is attached eagerly to each one of them and will 
		// provide interleaved results. 
//		Flux.merge(f1,f2,f3)
//		    .log()
//		    .subscribe(System.out::println);
		
//		System.out.println(" ---------- ");
//		// the doc says that subscribers are attached to the 1st source 
//		// on complete of first source, it subscribes to second source
//		// provide interleaved results. 
		Flux.concat(f1,f2,f3)
		  .log()
		  .subscribe(System.out::println);
				

		System.out.println("Putting main thread to sleep....");
		Thread.sleep(10000);
		
	}
	

}

class MyDataStructure<T1> {
	
	
	T1 t;
	
	public void someFunction(T1 t) {
		
	}
	
	
	public  <T2>  T2 someFunction2(T2 s) {
		
		
		
		return s;
	}
	
	
}
