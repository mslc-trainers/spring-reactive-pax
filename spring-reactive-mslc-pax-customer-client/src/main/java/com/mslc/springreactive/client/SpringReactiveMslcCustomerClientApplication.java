package com.mslc.springreactive.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringReactiveMslcCustomerClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringReactiveMslcCustomerClientApplication.class, args);
	}

}
