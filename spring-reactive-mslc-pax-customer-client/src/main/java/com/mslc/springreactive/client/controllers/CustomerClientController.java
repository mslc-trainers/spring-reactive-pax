package com.mslc.springreactive.client.controllers;

import static com.mslc.springreactive.client.controllers.APIConstants.GET_CUSTOMERS;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.mslc.springreactive.client.model.Customer;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class CustomerClientController {

	WebClient webClient = WebClient.create("http://localhost:8080");

	@GetMapping(path = GET_CUSTOMERS)
	public Flux<Customer> handleGetCustomers() {

		// @formatter:off

		return null;

	}

	@GetMapping(path = GET_CUSTOMERS + "/{id}")
	public Mono<Customer> handleGetCustomer(@PathVariable(name = "id") String customerId) {

		// @formatter:off

		return null;

	}

	@GetMapping(path = GET_CUSTOMERS + "/exchange")
	public Flux<Customer> handleGetCustomersWithExchange() {

		// @formatter:off

		return null;

	}

	@GetMapping(path = GET_CUSTOMERS + "/exchange/{id}")
	public Mono<Customer> handleGetCustomerWithExchange(@PathVariable(name = "id") String customerId) {

		// @formatter:off

		return null;

	}

	/**
	 * 
	 * curl -d '{"id":"999", "name":"GoldMan", "address":"Chicago"}' -H
	 * "Content-Type: application/json" -X POST http://localhost:8081/v1/customers
	 * 
	 * @param customerToCreate
	 * @return
	 */
	@PostMapping(path = GET_CUSTOMERS)
	public Mono<Customer> handlePostCustomer(@RequestBody Customer customerToCreate) {

		// @formatter:off

		return null;

	}

	/**
	 * curl -d '{"id":null, "name":"GoldMan New", "address":"Chicago IL"}' -H
	 * "Content-Type: application/json" -X PUT
	 * http://localhost:8081/v1/customers/101
	 * 
	 * @param id
	 * @param customerToUpdate
	 * @return
	 */
	@PutMapping(path = GET_CUSTOMERS + "/{id}")
	public Mono<Customer> handlePutCustomer(@PathVariable String id, @RequestBody Customer customerToUpdate) {

		// @formatter:off

		return null;

	}

	/**
	 * 
	 * curl -X DELETE http://localhost:8081/v1/customers/101
	 * 
	 * @param id
	 * @return
	 */
	@DeleteMapping(path = GET_CUSTOMERS + "/{id}")
	public Mono<Void> handleDeleteCustomer(@PathVariable String id) {

		// @formatter:off

		return null;

	}

}
