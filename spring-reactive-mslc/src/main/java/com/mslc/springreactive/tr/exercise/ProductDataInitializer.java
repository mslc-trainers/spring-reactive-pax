package com.mslc.springreactive.tr.exercise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.mslc.springreactive.model.Product;

import reactor.core.publisher.Flux;

@Component
//@Profile("!test")
public class ProductDataInitializer implements CommandLineRunner {

	public ProductDataInitializer() {

	}

	@Autowired
	ProductRepository productRepo;

	@Override
	public void run(String... args) throws Exception {

		initializeData();

	}

	private void initializeData() {

		// @formatter:off
		productRepo.deleteAll();

		Flux.just(new Product(1, "Laptop"), 
				  new Product(2, "Keyboard"), 
				  new Product(3, "Monitor"))
			.flatMap(x -> productRepo.save(x))
			.subscribe(x -> {
					System.out.println("Added Product : " + x + " added");
			});

	}

}
