package com.mslc.springreactive.tr.c2.controllers;

import java.time.Duration;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class FluxAndMonoController {

	@GetMapping(path = "/int-flux", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Flux<Integer> handleIntFluxRequest() {
		// @formatter:off
		return 
				Flux 
				   .just(1, 2, 3, 4, 5, 6);
//				   .delayElements(Duration.ofSeconds(1));
//				   .log();
		 
		// @formatter:on

	}

	@GetMapping(path = "/int-flux-stream", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
	public Flux<Long> handleIntFluxStreamRequest() {
		// @formatter:off
		
//		return Flux.interval(Duration.ofMillis(1000));
		
		return 
				Flux 
				.just(1L, 2L, 3L, 4L, 5L, 6L)
				.delayElements(Duration.ofSeconds(1));
//				.log();
		

	}

	@GetMapping(path = "int-mono")
	public Mono<Integer> handleIntMonoRequest() {

		return Mono.just(55).log();

	}

}
