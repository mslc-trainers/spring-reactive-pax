package com.mslc.springreactive.tr.exercise;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.mslc.springreactive.model.Product;

import reactor.core.publisher.Mono;

@RestController
public class ProductAPIRestController {

	@Autowired
	ProductRepository productRepo;
	
	Random r = new Random();

	@GetMapping(path = "/products/{productId}")
	public Mono<Product> handleGetProductById(@PathVariable Integer productId) {
		

		Mono<Product> product = productRepo.findById(productId);
		
		return product;

	}
	
	@GetMapping(path = "/products-from-cassandra/{productId}")
	public Mono<Product> handleGetProductByIdFromCassandra(@PathVariable Integer productId) {
		

		Mono<Product> product = productRepo.findById(productId);
		
		return product;

	}
	
	

}
