package com.mslc.springreactive.tr.c6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.mslc.springreactive.tr.c5.APIConstants;

@Configuration
public class CustomerAPIRouterConfig {

	@Bean
	public RouterFunction<ServerResponse> customersRoute(CustomerAPIHandler handler) {
	
		// @formatter:off

			return RouterFunctions
					.route(RequestPredicates
								.GET("/v2/customers"), 
								     handler::getAllCustomers)
					.andRoute(RequestPredicates
								.GET("/v2/customers/{id}"),
									handler::getCustomer)
					.andRoute(RequestPredicates.POST("/v2/customers"), handler::createCustomer)
					.andRoute(RequestPredicates.DELETE("/v2/customers/{id}"), handler::deleteCustomer)
					.andRoute(RequestPredicates.PUT("/v2/customers/{id}"), handler::updateCustomer);
			 
 	  // @formatter:on
			
	}

}
