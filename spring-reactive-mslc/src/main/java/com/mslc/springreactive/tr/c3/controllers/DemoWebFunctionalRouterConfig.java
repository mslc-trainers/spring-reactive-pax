package com.mslc.springreactive.tr.c3.controllers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicate;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class DemoWebFunctionalRouterConfig {

	@Bean
	public RouterFunction<ServerResponse> route(DemoWebFunctionalHandler handler) {

		// @formatter:off

//		return RouterFunctions
//				 .route()
//				 .GET("/ints-flux-functional", handler::fluxHandler)
//				 .build();
//				
		
	
		
		return RouterFunctions
				.route(RequestPredicates
						.GET("/ints-flux-functional")
						.and(RequestPredicates.accept(MediaType.APPLICATION_JSON)),
						      handler::fluxHandler)
				
				.andRoute(RequestPredicates
						.GET("/int-mono-functional")
						.and(RequestPredicates.accept(MediaType.APPLICATION_JSON)),
							  handler::monoHandler);
				
		 
		// @formatter:on

	}

}
