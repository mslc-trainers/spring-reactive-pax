package com.mslc.springreactive.tr.c3.controllers;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class DemoWebFunctionalHandler {

	public Mono<ServerResponse> fluxHandler(ServerRequest request) {
		
		
		

		
		// You will call your services here
	
		// @formatter:off
		return 
				ServerResponse.ok()
				    .contentType(MediaType.APPLICATION_JSON)
				    .body(
				    		Flux.just(1, 2, 3, 4, 5)
				    		    .log(), Integer.class
				    );
		// @formatter:on

	}

	public Mono<ServerResponse> monoHandler(ServerRequest request) {

		// @formatter:off
		
		
		return 
				ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(
						Mono.just(99)
						.log(), Integer.class
						);
		
		// @formatter:on

	}

}
