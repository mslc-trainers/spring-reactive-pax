package com.mslc.springreactive.tr.exercise;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.mslc.springreactive.model.Product;

@Repository
public interface ProductRepository extends ReactiveMongoRepository<Product, Integer> {

}
