package com.mslc.springreactive.tr.exercise;

import java.time.LocalDate;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mslc.springreactive.model.Invoice;

import reactor.core.publisher.Flux;

@RestController
public class InvoiceRestController {

	@GetMapping(path = "/invoices", params = { "customerId" })
	public Flux<Invoice> handleGetInvoices(@RequestParam String customerId) {

		/**
		 * private Customer customer; private CustomerAddress deliveryAddress; private
		 * LocalDate invoiceDate; private List<Product> products;
		 */

		if (customerId.equals("101")) {
			return Flux.just(new Invoice(1, "101", null, null, LocalDate.now(), null),
							 new Invoice(2, "101", null, null, LocalDate.now(), null),
							 new Invoice(3, "101", null, null, LocalDate.now(), null));
		} else {
			return Flux.just(new Invoice(4, "102", null, null, LocalDate.now(), null),
					 new Invoice(5, "102", null, null, LocalDate.now(), null),
					 new Invoice(6, "102", null, null, LocalDate.now(), null));
		}

	}

}
