package com.mslc.springreactive.tr.c4;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
public class CustomerAddress {

	@Id
	private String customerId;
	private String street;
	private String city;

	public CustomerAddress() {
		// TODO Auto-generated constructor stub
	}

	public CustomerAddress(String customerId, String street, String city) {
		
		this.customerId = customerId;
		this.street = street;
		this.city = city;
		
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	
	
}
