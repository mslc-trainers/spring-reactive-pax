package com.mslc.springreactive.routers;

import java.time.Duration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.mslc.springreactive.model.Training;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Configuration
public class TrainingRouter {

	@Bean
	public RouterFunction<ServerResponse> allTrainings() {

		return RouterFunctions.route(RequestPredicates.GET("/trainings"), this::getTrainings);
	}

	private Mono<ServerResponse> getTrainings(ServerRequest request) {

		// @formatter:off
		Mono<ServerResponse> response = ServerResponse
		  .ok()
		  .contentType(MediaType.APPLICATION_STREAM_JSON)
		  .body(this.generateTrainings(), Training.class);
		  
		  
		  
		// @formatter:on

		return response;
	}

	private Flux<Training> generateTrainings() {
		// @formatter:off

		
		return Flux.range(0, 30)
		   .onBackpressureDrop()
		   .map(x -> new Training(x, "Training :" + x + " - "))
		   .share();
//		
		// try out the following
//
//		return Flux.interval(Duration.ofMillis(1000))
//				   .map(x -> new Training(Integer.valueOf(x+""), "Training :" + x + " - "));
//				
//		
		
		// @formatter:on

	}

}
