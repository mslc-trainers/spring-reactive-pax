package com.mslc.springreactive.model;

import java.time.LocalDate;
import java.util.List;

import com.mslc.springreactive.tr.c4.Customer;
import com.mslc.springreactive.tr.c4.CustomerAddress;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data

public class Invoice {

	private int invoiceId;
	private String customerId;
	private Customer customer;
	private CustomerAddress deliveryAddress;
	private LocalDate invoiceDate;
	private List<Product> products;

	public Invoice() {
		// TODO Auto-generated constructor stub
	}

	public Invoice(int id, String customerId, Customer customer, CustomerAddress deliveryAddress, LocalDate invoiceDate,
			List<Product> products) {

		this.invoiceId = id;
		this.customerId = customerId;
		this.customer = customer;
		this.deliveryAddress = deliveryAddress;
		this.invoiceDate = invoiceDate;
		this.products = products;

	}

	public int getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(int invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public CustomerAddress getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(CustomerAddress deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public LocalDate getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(LocalDate invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

}
