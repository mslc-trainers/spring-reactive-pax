package com.mslc.springreactive.model;

public class Training {
	
	private Integer trainingId = 101;
	private String title = "Reactive";
	
	public Training() {
		// TODO Auto-generated constructor stub
	}
	
	
	public Training(Integer id, String name) {
		this.trainingId = id.intValue();
		this.title = name;
	}
	
	public Integer getTrainingId() {
		return trainingId;
	}
	public void setTrainingId(Integer trainingId) {
		this.trainingId = trainingId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

}
