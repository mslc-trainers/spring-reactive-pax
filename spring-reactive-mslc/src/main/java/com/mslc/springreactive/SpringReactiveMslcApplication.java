package com.mslc.springreactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringReactiveMslcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringReactiveMslcApplication.class, args);
	}

}
