package com.mslc.springreactive.s2;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.mslc.springreactive.model.Invoice;
import com.mslc.springreactive.util.DataBuilder;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class A1MoreWithFluxAndMono {

	@Test
	public void flux_single() {
		Flux<Integer> flux = Flux.fromIterable(Arrays.asList(1));
		flux = flux.map(x -> x * 100);

		// only if flux has one element or else either nosuchelemnt or indexoutofbounds
		// exception
		Mono<Integer> mono = flux.single();

		// @formatter:off

		StepVerifier
		   .create(flux)
		   .expectNext(100)
		   .expectComplete()
		   .verify();
		 
		
		
		assertThat(
				 mono.block())
				.isEqualByComparingTo(
						flux.blockLast());
		
		
		
	}
	
	@Test
	public void concat_2_mono_into_1_flux() {
		
		// @formatter:off
		
		Mono<Integer> m1 = Mono.just(10);
		Mono<Integer> m2 = Mono.just(2);
		m2 = m2.map(x -> x * 10);
		
		Flux<Integer> f1 = m1.concatWith(m2);
		
		StepVerifier.create(f1)
		      .expectNext(10)
		      .expectNext(20)
		      .expectComplete()
		      .verify();
	
	}
	
	

	@Test
	public void flux_take() {

		// @formatter:off
		
		
		
		Flux<Invoice> fluOfInvoices = Flux.fromIterable(DataBuilder.buildInvoices());
		
		List<Invoice> monoOfListOfInvoices = 
				fluOfInvoices
					.collectList()
					.block();

		// will stop consuming after 3rd element
		long start = System.currentTimeMillis();
		Flux.interval(Duration.ofMillis(1000))
		   .take(3)
		   .map(x -> {
			   System.out.println(x + " -- " + Thread.currentThread().getName());
			   return x;
		   })
		   .collectList()
		   .block()
		   .forEach(System.out::println);
		long end = System.currentTimeMillis();
		System.out.println("Time taken : " + (end - start));

	}

	
	@Test
	public void on_error_continue() {

		// @formatter:off

		Flux<Integer> f = 
				 Flux.range(0, 3)
				 .map(x -> {
					 
//					 System.out.println("processing....");
					 if (x == 0) {
						 
						 throw new RuntimeException("runtime...");
					 }
					 return x / 2;
				 })
				 .onErrorContinue((x, y) -> {
					
					 System.out.println("error :" + x.getMessage() + " -- " + y);
				 });
		
		
		f.subscribe(System.out::println);
		
		
	}
	
	@Test
	public void on_error_resume() {
		
		// @formatter:off
		
		Flux<Integer> f = 
				Flux.range(2, 10)
				.map(x -> {
					
					if (x == 4) {
						
						throw new RuntimeException("runtime...");
					}
					return x;
				})
				.onErrorResume(x -> {
					
					System.out.println("Error :" + x.getMessage());
					return Flux.just(5, 6, 7);
				});
		
		
		f.subscribe(System.out::println);
		
		
	}
	
	@Test
	public void on_error_return() {
		
		// @formatter:off
		
		Flux<Integer> f = 
				Flux.range(2, 10)
				.map(x -> {
					
					if (x == 4) {
						
						throw new RuntimeException("runtime...");
					}
					return x;
				})
				.onErrorReturn(5);
		
		
		f.subscribe(System.out::println);
		
		
	}
	
	
	@Test
	public void using_flux_zip() {

		int min = 0;
		int max = 4;
		// @formatter:off
		// 0, 2	
		Flux<Integer> evenNumbers = 
				Flux.range(min, max)
						.filter(x -> x % 2  == 0);		 
		 // 1, 3
		Flux<Integer> oddNumbers = 
				Flux.range(min, max)
				     .filter(x -> x % 2 > 0);
				
		Flux<Integer> fluxOfIntegers = Flux.zip(oddNumbers, evenNumbers, (a, b) -> a + b);
		// following is same stuff/
//		fluxOfIntegers = 	evenNumbers.zipWith(oddNumbers, (a, b) -> a + b);
		
		
		 StepVerifier.create(fluxOfIntegers)
			    .expectNext(1)
			    .expectNext(5)
			    .expectComplete()
			    .verify();
			  
			    
			// @formatter:on

	}

}
