package com.mslc.springreactive.tr.c1;

import java.time.Duration;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class A2MergingAndConcatingFlux {

	/**
	 * 
	 *  Merge, merges two fluxes and executes the processing in containing thread
	 */
	@Test
	public void when_merge_flux_uses_containing_thread() {

		// @formatter:off
		
		Flux<String> f1 = Flux.just("a", "b", "c");
		Flux<String> f2 = Flux.just("d", "e", "f");
		
		Flux<String> mergedFlux = Flux.merge(f1, f2);
		
//		mergedFlux.subscribe(System.out::println);
		StepVerifier.create(mergedFlux.log())
		.expectNextCount(6)
		.verifyComplete();
		
		
		// @formatter:on

	}

	
	/**
	 * 
	 * using delay results into using parrallelism. However, the order is not maintained
	 * 
	 */
	@Test
	public void when_merge_with_delayelements_flux_uses_parallel_threads() {

		// @formatter:off

		Flux<String> f1 = Flux.just("a", "b", "c").delayElements(Duration.ofSeconds(1));
		Flux<String> f2 = Flux.just("d", "e", "f").delayElements(Duration.ofSeconds(1));
		
		Flux<String> mergedFlux = Flux.merge(f1, f2);
		
		StepVerifier.create(mergedFlux.log())
					.expectNextCount(6)
//		    		.expectNext("a")
//		    		.expectNext("b")
//		    		.expectNext("c")
//		    		.expectNext("d")
//		    		.expectNext("e")
//		    		.expectNext("f")
		    		.expectComplete()
		    		.verify();
		    
		 // @formatter:on

	}

	/**
	 * use concat in order to maintain the order. but this will result in slow processing though it does parallel processing
	 * 
	 */
	@Test
	public void when_concat_with_delayelements_flux_maintains_order_but_slow() {

		// @formatter:off
		
		Flux<String> f1 = Flux.just("a", "b", "c").delayElements(Duration.ofSeconds(1));
		Flux<String> f2 = Flux.just("d", "e", "f", "g").delayElements(Duration.ofSeconds(1));
		
		Flux<String> mergedFlux = Flux.concat(f1, f2);
		
		StepVerifier.create(mergedFlux.log())
					.expectSubscription()
					.expectNextCount(7)
//		    		.expectNext("a")
//		    		.expectNext("b")
//		    		.expectNext("c")
//		    		.expectNext("d")
//		    		.expectNext("e")
//		    		.expectNext("f")
		.expectComplete()
		.verify();
		
		// @formatter:on

	}

	/**
	 * Use delayElements in order to delay the onNext.
	 * <hr>
	 * then use Flux.zip in order concatenate elements from each flux. You will have
	 * to use a BiConsumer to provide the concatenation mechanism
	 * 
	 * 
	 * 
	 * 
	 */
	@Test
	public void when_zip_flux_uses_parallel_threads() {

		// @formatter:off
		
		Flux<String> f1 = Flux.just("a", "b", "c").delayElements(Duration.ofSeconds(1));
		Flux<String> f2 = Flux.just("d", "e", "f", "g").delayElements(Duration.ofSeconds(1));
		Flux<Integer> f3 = Flux.just(1);
		
		Flux<String> mergedFlux = Flux.zip(f1, f3, (x, y) -> x + y);
		Optional<String> o = null;
		
	
		
		StepVerifier.create(mergedFlux.log())
		.expectSubscription()
		.expectNextCount(4)
//		    		.expectNext("a")
//		    		.expectNext("b")
//		    		.expectNext("c")
//		    		.expectNext("d")
//		    		.expectNext("e")
//		    		.expectNext("f")
		.expectComplete()
		.verify();
		
		// @formatter:on

	}

}
