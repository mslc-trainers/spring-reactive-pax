package com.mslc.springreactive.tr.c1;

import java.time.Duration;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;

public class A6ColdAndHotStream {

	@Test
	public void when_cold_stream() throws InterruptedException {

		// @formatter:off

		Flux<String> stringFlux = 
				Flux.just("a", "b", "c", "d")
				.delayElements(Duration.ofSeconds(1))
				.log();
				
		 
		// @formatter:on

		stringFlux.subscribe(System.out::println);
		System.out.println(" **** ");
		stringFlux.subscribe(System.out::println);
		
		System.out.println("Subscription Done");
		
		Thread.sleep(4000);

	}

	@Test
	public void when_hot_stream() throws InterruptedException {

		// @formatter:off

		Flux<String> stringFlux = 
				Flux.just("a", "b", "c", "d", "e")
				.delayElements(Duration.ofSeconds(1));
		
//		stringFlux.subscribe(System.out::println);
		
		 
		// @formatter:on

		
		ConnectableFlux<String> connectableFlux = stringFlux.publish();

		// connect will execute the flux
		connectableFlux.connect();

		// by the time it reaches here, there are no elements left 
		connectableFlux
		   .subscribe(x -> System.out.println(x + " subscriber 1"));
		Thread.sleep(3000);
		System.out.println(" **** --- ****");
		
		connectableFlux
		  .subscribe(x -> System.out.println(x + " subscriber 2"));
		Thread.sleep(5000);
		

	}
}
