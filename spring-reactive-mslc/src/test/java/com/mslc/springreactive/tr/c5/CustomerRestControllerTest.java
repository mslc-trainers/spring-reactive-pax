package com.mslc.springreactive.tr.c5;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.mslc.springreactive.tr.c4.Customer;
import com.mslc.springreactive.tr.c4.CustomerRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;


@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureWebTestClient
@TestInstance(Lifecycle.PER_CLASS)
@ActiveProfiles("test")
@TestMethodOrder(OrderAnnotation.class) 
public class CustomerRestControllerTest {

	@Autowired
	WebTestClient webTestClient;

	/**
	 * 
	 * Repo is bound only to build test data
	 */
	@Autowired
	CustomerRepository customerRepo;

	private List<Customer> data() {

		// @formatter:off

		return Arrays.asList(
				new Customer("101", "IBM", "Bangalore"), 
				new Customer("102", "BNP Pariba", "Mumbai"),
				new Customer("103", "Morgan Stanley", "Hyderabad"), 
				new Customer("104", "JPMC", "Hyderabad")
				);
		 
	}

	@BeforeAll
	public void setUp() {
		// @formatter:off

		customerRepo.deleteAll()
		    .thenMany(Flux.fromIterable(data()))
		    .flatMap(x -> {
		    	return customerRepo.save(x);
		    })
		    .doOnNext(x -> {
		    	System.out.println("added :" + x.getId()  + " -- " + x.getName());
		    }).blockLast();
		 
		// @formatter:on

	}

	@Test
	@Order(1)
	public void test_get_customers() {
		
		// @formatter:off
		webTestClient.get()
				.uri(APIConstants.GET_CUSTOMERS)
				.accept(MediaType.APPLICATION_JSON)
				.exchange()
				.expectBodyList(Customer.class)
				.hasSize(4);
			
	}
	
	@Test
	@Order(1)
	public void test_get_customers_using_consume_with() {
		
		// @formatter:off
		
		webTestClient.get()
		.uri(APIConstants.GET_CUSTOMERS)
		.accept(MediaType.APPLICATION_JSON)
		.exchange()
		.expectBodyList(Customer.class)
		.hasSize(4)
		.consumeWith(x -> {

			List<Customer> customers =  x.getResponseBody();
			customers.forEach(y -> {
				assertTrue(y.getId() != null);
			});
		});
		
		// @formatter:on
		
		
	}
	@Test
	@Order(1)
	public void test_get_customers_using_return_result() {
		
		// @formatter:off
		
	Flux<Customer> customers = 	webTestClient.get()
		.uri(APIConstants.GET_CUSTOMERS)
		.accept(MediaType.APPLICATION_JSON)
		.exchange()
		.returnResult(Customer.class)
		.getResponseBody();
	
	StepVerifier.create(customers)
			.expectNextCount(4)
			.verifyComplete();
		
		// @formatter:on
		
	}
	
	@Test
	@Order(2)
	public void test_get_customerById_then_return_customer() {
		
		// @formatter:off

	 EntityExchangeResult<Customer> customer =	webTestClient.get()
		    .uri(APIConstants.GET_CUSTOMERS+"/101")
		    .accept(MediaType.APPLICATION_JSON)
		    .exchange()
		    .expectStatus().isOk()
		    .expectBody(Customer.class)
		    .returnResult();
		    
		assertTrue(customer.getResponseBody().getName().equals("IBM"));
		// @formatter:on

		
	}
	@Test
	@Order(2)
	public void test_get_customerById_with_response_entity_then_return_customer() {
		
		// @formatter:off
		
		webTestClient.get()
				.uri(APIConstants.GET_CUSTOMERS+"/with-response-entity/{id}", "101")
				.exchange()
				.expectStatus().isOk()
				.expectBody()
				.jsonPath("$.name").isEqualTo("IBM");
		
//		assertTrue(customer.getResponseBody().getName().equals("IBM"));
		// @formatter:on
		
		
	}
	
	@Test
	public void test_get_customerById_not_found() {
		
		// @formatter:off
		
		webTestClient.get()
		.uri(APIConstants.GET_CUSTOMERS+"/with-response-entity/{id}", "555")
		.exchange()
		.expectStatus().isNotFound();
		
//		assertTrue(customer.getResponseBody().getName().equals("IBM"));
		// @formatter:on
		
		
	}
	
	@Test
	public void test_create_new_customer() {
		
		// @formatter:off
		
		Customer customer = new Customer("555", "Bank of America", "Chennai" );
		
		Flux<Customer> newCustomer =
				webTestClient
					.post()
					.uri(APIConstants.GET_CUSTOMERS)
					.contentType(MediaType.APPLICATION_JSON)
					.body(Mono.just(customer), Customer.class)
					.exchange()
					.expectStatus().isCreated()
					.returnResult(Customer.class)
					.getResponseBody();
		
		
			StepVerifier.create(newCustomer)
					.expectNextMatches(x -> x.getName().equals("Bank of America"))
					.verifyComplete();
		
		
//		assertTrue(customer.getResponseBody().getName().equals("IBM"));
		// @formatter:on
		
		
	}
	
	@Test
	@Order(99)
	public void test_delete_customer() {
		
		// @formatter:off
		
				webTestClient
				.delete()
				.uri(APIConstants.GET_CUSTOMERS+"/101")
				.exchange()
				.expectStatus().isOk()
				.expectBody(Void.class);
				
//		assertTrue(customer.getResponseBody().getName().equals("IBM"));
		// @formatter:on
		
		
	}
	
	@Test
	@Order(45)
	public void test_update_customer() {
		
		// @formatter:off
		
		Customer customer = new Customer("101", "IBM India", "Delhi");
		
		webTestClient
		.put()
			.uri(APIConstants.GET_CUSTOMERS+"/101")
			.contentType(MediaType.APPLICATION_JSON)
			.body(Mono.just(customer), Customer.class)
			.exchange()
			.expectStatus().isOk()
			.expectBody(Customer.class)
			.consumeWith(x -> {
				
				Customer updatedCustomer = x.getResponseBody();
				assertTrue(updatedCustomer.getName().equals("IBM India") && updatedCustomer.getAddress().equals("Delhi"));
				
			});
		
//		assertTrue(customer.getResponseBody().getName().equals("IBM"));
		// @formatter:on
		
		
	}
	@Test
	@Order(45)
	public void test_update_invalid_customer() {
		
		// @formatter:off
		
		Customer customer = new Customer("101", "IBM India", "Delhi");
		
		webTestClient
			.put()
			.uri(APIConstants.GET_CUSTOMERS+"/999")
			.contentType(MediaType.APPLICATION_JSON)
			.body(Mono.just(customer), Customer.class)
			.exchange()
			.expectStatus().isNotFound();
		
//		assertTrue(customer.getResponseBody().getName().equals("IBM"));
		// @formatter:on
		
		
	}

}
