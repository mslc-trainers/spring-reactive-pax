package com.mslc.springreactive.tr.c6;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.mslc.springreactive.tr.c4.Customer;
import com.mslc.springreactive.tr.c4.CustomerRepository;
import com.mslc.springreactive.tr.c5.APIConstants;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureWebTestClient
@TestInstance(Lifecycle.PER_CLASS)
@ActiveProfiles("test")
@TestMethodOrder(OrderAnnotation.class)
public class CustomerAPIHandlerTest {

	@Autowired
	WebTestClient webTestClient;

	@Autowired
	CustomerRepository customerRepo;

	private List<Customer> data() {

		// @formatter:off

		return Arrays.asList(
				new Customer("101", "IBM", "Bangalore"), 
				new Customer("102", "BNP Pariba", "Mumbai"),
				new Customer("103", "Morgan Stanley", "Hyderabad"), 
				new Customer("104", "JPMC", "Hyderabad")
				);
		 
	// @formatter:on

	}

	@BeforeAll
	public void setUp() {
		// @formatter:off

		customerRepo.deleteAll()
		    .thenMany(Flux.fromIterable(data()))
		    .flatMap(x -> {
		    	return customerRepo.save(x);
		    })
		    .doOnNext(x -> {
		    	System.out.println("added :" + x.getId()  + " -- " + x.getName());
		    }).blockLast();
		 
		// @formatter:on

	}

	@Test
	@Order(1)
	public void when_get_customers_then_return_customers() {

		// @formatter:off

		webTestClient.get()
				.uri(APIConstants.GET_CUSTOMERS_FUNCTIONAL)
				.accept(MediaType.APPLICATION_JSON)
				.exchange()
				.expectBodyList(Customer.class)
				.hasSize(4);
				
		// @formatter:on

	}

	@Test
	@Order(2)
	public void when_get_customer_by_customerId_then_return_customer() {

		// @formatter:off
		
			webTestClient.get()
				.uri(APIConstants.GET_CUSTOMERS_FUNCTIONAL+"/{id}", "101")
				.accept(MediaType.APPLICATION_JSON)
				.exchange()
				.expectStatus().isOk()
				.expectBody(Customer.class)
				.consumeWith(x -> {
					assertTrue(x.getResponseBody().getName().equals("IBM"));
				});
			
		// @formatter:on

	}

	@Test
	@Order(3)
	public void when_invalid_get_customer_then_notfound() {

		// @formatter:off
		
		webTestClient.get()
		.uri(APIConstants.GET_CUSTOMERS_FUNCTIONAL+"/{id}", "1111")
		.accept(MediaType.APPLICATION_JSON)
		.exchange()
		.expectStatus().isNotFound();
		// @formatter:on

	}

	@Test
	@Order(4)
	public void when_create_customer_get_new_customer() {

		Customer c = new Customer("505", "Nomura", "Mumbai");
		// @formatter:off
		
		Flux<Customer> newCustomer =	webTestClient.post()
				.uri(APIConstants.GET_CUSTOMERS_FUNCTIONAL)
				.contentType(MediaType.APPLICATION_JSON)
				.body(Mono.just(c), Customer.class)
				.exchange()
				.expectStatus().isOk()
				.returnResult(Customer.class)
				.getResponseBody();
			
		StepVerifier
		    .create(newCustomer)
		    .expectNextMatches(x -> x.getAddress().equals("Mumbai"))
		    .verifyComplete();
		// @formatter:on

	}

	@Test
	@Order(5)
	public void when_update_customer() {

		// @formatter:off

		 Customer customer = new Customer("101", "IBM India Ltd", "Goa");
		 Flux<Customer> updatedCusomer =	webTestClient.put()
			   .uri(APIConstants.GET_CUSTOMERS_FUNCTIONAL+"/{id}", "101")
			   .contentType(MediaType.APPLICATION_JSON)
			   .accept(MediaType.APPLICATION_JSON)
			   .body(Mono.just(customer), Customer.class)
			   .exchange()
			   .returnResult(Customer.class)
			   .getResponseBody();
		 
		 StepVerifier.create(updatedCusomer)
		 			.expectNextMatches(x -> x.getName().equals("IBM India Ltd"))
		 			.verifyComplete();
		   
		// @formatter:on

	}

	@Test
	@Order(6)
	public void when_delete_customer() {

		// @formatter:off

		webTestClient.delete()
			   .uri(APIConstants.GET_CUSTOMERS_FUNCTIONAL + "/{id}", "101")
			   .accept(MediaType.APPLICATION_JSON)
			   .exchange()
			   .expectStatus().isOk()
			   .expectBody(Void.class);
			
		   
		// @formatter:on

	}

}
