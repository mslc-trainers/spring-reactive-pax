package com.mslc.springreactive.tr.c1;

import java.sql.SQLException;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import com.mslc.springreactive.model.Invoice;
import com.mslc.springreactive.model.Product;
import com.mslc.springreactive.util.DataBuilder;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;

/**
 * Just a peek into Flux and Mono and then take a look at the the deck
 * 
 * 
 * @author muhammedshakir
 *
 */
public class A1MoreWithFluxErrorHandlingAndTesting {

	List<String> myClients = Arrays.asList("JPMC", "Nomura", "Morgan Stanley", "BNP Pariba", "BoA");

	/**
	 * 
	 * Create flux with .just and the concat the same with another flux agains
	 * created with Flux.just
	 * 
	 * Using concatWith to concat with Flux.error as well log it and see the results
	 * 
	 * Use subscribe to swing the Flux in action
	 * 
	 * 
	 */
	@Test
	public void flux_contactWith() {

		// @formatter:off
		
		Flux<String> stringFlux = 
				  Flux.fromIterable(myClients)
//				        .concatWith(Flux.error(() -> new RuntimeException()))
				        .concatWith(Flux.just("BNP Pariba"))
				        .log();
		
		StepVerifier.create(stringFlux)
		            .expectNextCount(6)
		            .verifyComplete();
	
		stringFlux.subscribe(value -> System.out.println(value),
							 error -> System.out.println(error),
							 new Runnable() {
									@Override
									public void run() {
										System.out.println("Completed...");
									}
							 });
		
		System.out.println(" ----- ");
		stringFlux.subscribe(value -> System.out.println(value),
				 error -> System.out.println(error),
				 () -> {System.out.println("Completed...");});		
		 
		// @formatter:on

	}

	/**
	 * Create flux of strings concatWith Flux.error Log
	 * <ol>
	 * <li>Use StepVerifier in order to test</li>
	 * <li>Use expectNext</li>
	 * <li>Use expectError</li> Use verify()</li>
	 * </ol>
	 */
	@Test
	public void flux_test_element_with_error() {

		// following - The resultant Flux can be assigned to Flux of anything
//		Flux<Integer> ff = Flux.error(() -> new RuntimeException());

		// @formatter:off
		
		Flux<String> stringFlux = Flux.fromIterable(myClients)
				   .concatWith(Flux.error(() -> new RuntimeException()))
				   .log();

		
		// following subscribe will throw an exception as there is no callBackHandler in case of error
		// stringFlux.subscribe(System.out::println);
		
		// Since there is a subscriber used by StepVerifier, the exception will still be 
		// thrown on the console but test case will pass because of expectError()
		StepVerifier.create(stringFlux)
		             .expectNext("JPMC")
		             .expectNext("Nomura")
		             .expectNext("Morgan Stanley")
		             .expectNext("BNP Pariba")
		             .expectNext("BoA")
		             .expectError()
		             .verify();
		             
		// @formatter:on

	}

	/**
	 * 
	 * Create flux
	 * <ol>
	 * <li>use StepVerifier</li>
	 * <li>use expectNextCount()</li>
	 * </ol>
	 */
	@Test
	public void flux_test_elements_with_expect_count() {

		// @formatter:off
 

		Flux<String> stringFlux = Flux.fromIterable(myClients)
				    .concatWith(Flux.error(new RuntimeException()))
				    .log();
		
//		StepVerifier.create(stringFlux)
//		  			.expectNextCount(3)
//		  			.expectError()
//		  			.verify();
		
		StepVerifier.create(stringFlux)
			.expectNextCount(3)
			.expectNextCount(2)
			.expectError()
			.verify();
		
		StepVerifier.create(stringFlux)
		.expectNextCount(5)
		.expectError()
		.verify();
		
		// @formatter:on
	}

	/**
	 * 
	 * Create flux
	 * <ol>
	 * <li>use StepVerifier</li>
	 * <li>use expectNextCount()</li>
	 * <li>use expectErrorMessage()</li>
	 * </ol>
	 */
	@Test
	public void flux_test_elements_with_expect_error_message() {

		// @formatter:off
		
		
		Flux<String> stringFlux = Flux.fromIterable(myClients)
				.concatWith(Flux.error(new RuntimeException("Insufficient Balance")))
				.log();

		
		StepVerifier.create(stringFlux)
		.expectNextCount(5)
		.expectErrorMessage("Insufficient Balance")
		.verify();
		
	
		// @formatter:on
	}

	/**
	 * 
	 * You can create a Flux or a Mono and directly pass the same to the
	 * StepVerifier
	 */
	@Test
	public void mono_error() {

		// you may want to use .log as well
		// @formatter:off
		StepVerifier
		     .create(Mono.just("InfoMover")
		    		 .concatWith(Mono.error(new RuntimeException()))
		    		 				 )
		     .expectNext("InfoMover")
		     .expectError()
		     .verify();
		
		 
		// @formatter:on

	}

	@Test
	public void when_repeat() {

		Flux<Integer> ints = Flux.just(1, 2, 3).map(x -> x + 1)
				// 3 additional times
				// not specifying count means : indefinitely
				.repeat(3, () -> true);

		ints.subscribe(System.out::println);
		StepVerifier.create(ints).expectNextCount(12).verifyComplete();

	}

	/**
	 * 
	 * use map to covert string to its respective length
	 * <ol>
	 * <li>use map</li>
	 * <li>use repeat</li>
	 * <li>repeat() will result in infinite repeat</li>
	 * <li>repeat(1) will result in 1 time repeat of subscription</li>
	 * <li>use map</li>
	 * </ol>
	 * 
	 */
	@Test
	public void using_map_on_flux() {
		List<String> myClients = Arrays.asList("JPMC", "Nomura", "Morgan Stanley", "BNP Pariba", "BoA");
		// 4, 6, 14, 10, 3
		// @formatter:off

		Flux<Integer> myClientsNameLengths = 
				  Flux.fromIterable(myClients)
				  .map(x -> {
					  
					 // System.out.println(x.length());
					  return x.length();
				  })
				  .repeat(2);
		
		myClientsNameLengths
		     .subscribe(System.out::println,
		    		 	System.out::println,
		    		 	() -> {System.out.println("complete...");});
		
		
		StepVerifier.create(myClientsNameLengths)
					.expectNext(4, 6, 14, 10, 3, 4, 6, 14, 10, 3,4, 6, 14, 10, 3)
					.expectComplete()
					.verify();
		 
		// @formatter:on

	}

	@Test
	public void when_flatMap_with_streams() {

		Stream<String> names = Stream.of("a", "b", "c");

		// @formatter:off
		               names
		                .map(x -> x.toUpperCase())
		                .collect(Collectors.toList());
		               

		 List<Invoice> invoices = DataBuilder.buildInvoices();
		               
//		 List<Invoice> invoices = Arrays
//				    .asList(new Invoice(101, LocalDate.now(), 
//				    		    Arrays.asList(new Product(101, "Mouse"),
//				    		    		      new Product(102, "Keyboard"))),
//				    		new Invoice(101, LocalDate.now(), Arrays.asList(new Product(105, "laptop"))));

		 
//	List<List<Product>> ids = 	
//			invoices
//			    .stream()
//			    .map(x -> x.getProducts())
//			    .collect(Collectors.toList());
		 
//			List<Product> ids = 	
//					invoices
//					    .stream()
//					    .flatMap(x -> x.getProducts().stream())
//					    .collect(Collectors.toList());
		 
		 
		
		 List<Product> ids = invoices
				    .stream()
				    .flatMap(x -> x.getProducts().stream())
				    .collect(Collectors.toList());
		 
		 Flux<Invoice> invoice2 = 
				   Flux.fromIterable(invoices);
		 
		 Flux<Product> productFlux  = 
				      invoice2
				         .flatMap(x -> Flux.fromIterable(x.getProducts()));
		  productFlux
		      .subscribe(x -> System.out.println(x.getName()));
		             
		             	
		 
		 
		 
		 
		 
		 
			                
		// @formatter:on

	}

	/**
	 * 
	 * Using flatMap to convert to process list
	 *
	 * 
	 */
	@Test
	public void using_flatmap_on_flux() {

		// @formatter:off

		 Flux<String> stringFlux = 
				 Flux
				   .fromIterable(Arrays.asList("A", "B", "C", "D", "E", "F"))
				   .flatMap(x -> {
					   
					   Flux<String> concatenatedFlux = Flux.fromIterable(concatenateItem(x)); 
					   
					   return concatenatedFlux; 
					   
				   }).log();
		 
//		 stringFlux.subscribe();
		 
		 StepVerifier.create(stringFlux)
		    		.expectNextCount(12)
		    		.verifyComplete();
		// @formatter:on

	}

	/**
	 * Step by step
	 * <h2>First understand the concept of window</h2>
	 * <ol>
	 * <li>Create flux of string of 3 strings</li>
	 * <li>use log</li>
	 * <li>use window(1)</li>
	 * <li>note that you will get Flux<Flux<String>></li>
	 * <li>Use a StepVerifier to asser that the Flux<Flux<String>> has 3
	 * elements</li>
	 * <li>Change to window(2) - <b>now you will get 2 elements</li>
	 * </ol>
	 * 
	 * 
	 */
	@Test
	public void using_flatmap_on_flux_parallel() {

		// @formatter:off

//		Flux<String> stringFlux = 
//				Flux
//				.fromIterable(Arrays.asList("A", "B", "C", "D", "E", "F"))
//				.window(2)
//				.flatMap(x -> 
//						{
////								
////							Flux<List<String>> concatenatedFluxofList = 
////									x.map(this::concatenateItem)
////									.subscribeOn(Schedulers.parallel());
////							
////							Flux<String> concatenatedFlux = 
////									concatenatedFluxofList
////									.flatMap( y -> Flux.fromIterable(y));
//							
////							return concatenatedFlux;
//							return x.map(this::concatenateItem)
//							   .subscribeOn(Schedulers.parallel())
//							   .flatMap(y -> Flux.fromIterable(y)) ;
//					})
//				.log();				   
//				
//		   stringFlux.subscribe(System.out::println, System.out::println, ()-> {System.out.println("complete");});
		
		
		Flux<String> stringFlux = 
				Flux
				.fromIterable(Arrays.asList("A", "B", "C", "D", "E", "F"))
				.window(2)
				.flatMap(x -> 
						 x.map(this::concatenateItem)
							   .subscribeOn(Schedulers.parallel())
							   .flatMap(y -> Flux.fromIterable(y))
					)
				.log();				   
				
		
		
//		
		StepVerifier.create(stringFlux)
		 			.expectNextCount(12)
		 			.verifyComplete();	
		
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		// @formatter:on

	}

	private List<String> concatenateItem(String s) {

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Arrays.asList(s, "1");
	}

	@Test
	public void handle_error_using_on_error_map() {
		// @formatter:off
		Flux<String> stringFlux = 
				Flux.just("a", "b", "c")
				.concatWith(Flux.error(new RuntimeException("Runtime exception....")))
				.concatWith(Flux.just("Goldman Sachs"))
				.onErrorMap( x -> new SQLException(x.getMessage() + " - sql error"))
				.log();
		
		
		StepVerifier
			.create(stringFlux)
			.expectNextCount(3)
			.expectError(SQLException.class)
			.verify();
		
		
		// @formatter:on

	}

	/**
	 * use onErrorMap to convert the exception. Also use retry Also use expectError
	 * in StepVerifier
	 * <hr>
	 * .retry will retry the processing and then onErrorMap will be executed <b> :
	 * Note that retry works only when error is actuall thrown. The situations where
	 * errors are handled gracefully like (onErrorReturn), it will not work
	 */
	@Test
	public void handle_error_using_on_error_map_with_retry() {
		// @formatter:off
		Flux<String> stringFlux = 
				Flux.just("a", "b", "c")
				.concatWith(Flux.error(new RuntimeException("Runtime exception....")))
				.concatWith(Flux.just("Goldman Sachs"))
				.onErrorMap( x -> new SQLException(x + " - sql error"))
				.retry(1)
				.log();
		
		
		StepVerifier
			.create(stringFlux)
			.expectNextCount(3)
			.expectNextCount(3)
			.expectError(SQLException.class)
			.verify();
		
		// @formatter:on

	}

	/**
	 * use onErrorMap to convert the exception. Also use retry Also use expectError
	 * in StepVerifier
	 * <hr>
	 * .retryWithBackOff will do a retry after the duration specified
	 */
	@Test
	public void handle_error_using_on_error_map_with_retryBackOff() {
		// @formatter:off
		Flux<String> stringFlux = 
				Flux.just("a", "b", "c")
				.concatWith(Flux.error(new RuntimeException("Runtime exception....")))
				.concatWith(Flux.just("Goldman Sachs"))
				.onErrorMap( x -> new SQLException(x + " - sql error"))
				.retryBackoff(1, Duration.ofMillis(1500))
				.log();
		
		
		StepVerifier
		.create(stringFlux)
		.expectNextCount(3)
		.expectNextCount(3)
		.expectError(IllegalStateException.class)
		.verify();
		
		
		// @formatter:on

	}

}
