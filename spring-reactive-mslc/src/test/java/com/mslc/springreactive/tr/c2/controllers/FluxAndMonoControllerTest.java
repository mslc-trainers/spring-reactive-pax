package com.mslc.springreactive.tr.c2.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@WebFluxTest
public class FluxAndMonoControllerTest {

	@Autowired
	WebTestClient webTestClient;

	@Test
	public void playing_with_response() {

		// @formatter:off

		Flux<Integer> f =	webTestClient
			   .get()
			   .uri("/int-flux")
			   .accept(MediaType.APPLICATION_JSON)
			   .exchange()
			   .expectStatus().isOk()
			   .returnResult(Integer.class)
			   .getResponseBody();
		
		f.subscribe(System.out::println);
		   
		   
		   
		   
		   // expectHeader
//		   .expectHeader().contentType(MediaType.APPLICATION_JSON);
		   
		   // following will fail because the datatype is JSON array and not 
		   // just 1 integer
//		   .expectBody(new ParameterizedTypeReference<Integer>() {
//			})
//		   .consumeWith(x -> {
//			   System.out.println(x.getResponseBody());
//		   });
		   
		   
		   
		   
		   // using expectBodyList with ParameterizedTypeReference
		   // if typecast is possible then it will be success or else it will fail
//		   .expectBodyList(new ParameterizedTypeReference<Integer>() {
//		    })
//		   .consumeWith(x -> {
//			   
//			   System.out.println("List : " + x.getResponseBody());
//		   });
	
		   
		   
		   // using expectBodyList
//		   .expectBodyList(Integer.class)
//		   .consumeWith(x -> {
//			   
//			   System.out.println(x.getResponseBody());
//		   });
		   
		   // use expectBody with data type
//		   .expectBody(Integer[].class)
//		   .consumeWith(x -> {
//			   Integer[] ints = x.getResponseBody();
//			   System.out.println(Arrays.asList(ints)); 
//		   });
		   
		   
		   // using expectBody
//		   .expectBody()
//		   .consumeWith(x -> {
//			   byte[] response = x.getResponseBody();
//			   String data = new String(response);
//			   System.out.println(data);
//		   });
		
		   
		   
	}
	
	@Test
	public void when_int_flux_get_ints() {

		// @formatter:off
		Flux<Integer> allInts = webTestClient
		    .get()
		    .uri("/int-flux")
		    .accept(MediaType.APPLICATION_JSON)
		    .exchange()
		    .expectStatus().isOk()		    
		    .returnResult(Integer.class)
		    .getResponseBody()
		    .log();
		
		
		StepVerifier
		      .create(allInts)
		      .expectSubscription()
		      .expectNextCount(6)
		      .verifyComplete();
		
//		allInts.subscribe(System.out::println);

		 
		// @formatter:on

	}

	@Test
	public void when_int_flux_get_ints_using_expectBodyList() {

		// @formatter:off

		webTestClient
		     .get()
		     .uri("/int-flux")
		     .accept(MediaType.APPLICATION_JSON)
		     .exchange()
		     .expectStatus().isOk()
		     .expectHeader().contentType(MediaType.APPLICATION_JSON)
		     .expectBodyList(Integer.class)
		     .hasSize(6);
		// @formatter:on

	}

	@Test
	public void when_int_flux_get_ints_using_expectBodyList_returnResult() {

		List<Integer> expectedValues = Arrays.asList(1, 2, 3, 4, 5, 6);
		// @formatter:off
		
			List<Integer> values=	webTestClient
				.get()
				.uri("/int-flux")
				.accept(MediaType.APPLICATION_JSON)
				.exchange()
				.expectStatus().isOk()
				.expectHeader().contentType(MediaType.APPLICATION_JSON)
				.expectBodyList(Integer.class)
				.returnResult()
				.getResponseBody();
		// @formatter:on

		assertEquals(expectedValues, values);

	}

	/**
	 * 
	 * Exercise for you - use consumeWith
	 */
	@Test
	public void when_int_flux_get_ints_using_expectBodyList_consumeWith() {

		List<Integer> expectedValues = Arrays.asList(1, 2, 3, 4, 5, 6);
		// @formatter:off
		
		webTestClient
				.get()
				.uri("/int-flux")
				.accept(MediaType.APPLICATION_JSON)
				.exchange()
				.expectStatus().isOk()
				.expectHeader().contentType(MediaType.APPLICATION_JSON)
				.expectBodyList(Integer.class)
				.consumeWith(x -> {
					
					assertEquals(x.getResponseBody(), expectedValues);
				});
		// @formatter:on

	}

	@Test
	public void when_stream_then_use_cancel() {
		// @formatter:off

		
		Flux<Long> longValues =	webTestClient
			    .get()
			    .uri("/int-flux-stream")
			    .accept(MediaType.APPLICATION_STREAM_JSON)
			    .exchange()
//			    .expectHeader().contentType(MediaType.APPLICATION_STREAM_JSON)
			    .returnResult(Long.class)
			    .getResponseBody();
			
//			    
//		StepVerifier
//		    .create(longValues.log())
//		    .expectNext(1L)
//		    .expectNext(2L)
//		    .expectNext(3L)
//		    .expectNext(4L)
//		    .expectNext(5L)
//		    .expectNext(6L)
//		    .verifyComplete();		
		
		
	    
		StepVerifier
		    .create(longValues.log())
		    .expectNext(1L)
		    .expectNext(2L)
		    .expectNext(3L)
		    .thenCancel()
		    .verify();		
		    
			    
				
				
				
				 
	    // @formatter:on

	}

	@Test
	public void when_mono() {
		// @formatter:off
//
			Flux<Integer> ints =	webTestClient
					.get()
					.uri("/int-mono")
					.accept(MediaType.ALL)
					.exchange()
					.returnResult(Integer.class)
					.getResponseBody();
			
			Mono<Integer> theMono = ints.elementAt(0);
//			theMono.subscribe(System.out::println);
//			Integer i  = ints.blockLast();
//			assertEquals(i, 55);
			
			StepVerifier.create(theMono)
						.expectNext(55)
						.verifyComplete();
			
			
			
			webTestClient
					.get()
					.uri("/int-mono")
					.accept(MediaType.ALL)
					.exchange()
					.expectBody(Integer.class)
					.consumeWith(x -> {
						
						assertEquals(x.getResponseBody(), 55);
					});
		 
		// @formatter:on

	}

}
