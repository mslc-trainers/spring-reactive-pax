package com.mslc.springreactive.tr.c4;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@DataMongoTest
@RunWith(SpringRunner.class)
@TestInstance(Lifecycle.PER_CLASS)
public class CustomerRepositoryTest {

	Logger _log = LoggerFactory.getLogger(CustomerRepositoryTest.class);

	// @formatter:off

		static List<Customer> testData = Arrays
				.asList(new Customer("1", "JPMC", "Mumbai"),
						new Customer(null, "Morgan Stanley", "Mumbai"), 
						new Customer(null, "BNP Pariba", "Mumbai"));
	 
	// @formatter:on

	@Autowired
	CustomerRepository customerRepo;

	@BeforeAll
	public void setUp() {

		/**
		 * blockLast : Subscribe to this Flux and block indefinitely until the upstream signals its
		 * last value or completes. 
		 * 
		 */

		// @formatter:off

//		customerRepo
//			.deleteAll()
//			.thenMany(Flux.fromIterable(testData))
//			.flatMap(x -> customerRepo.save(x))
//			.doOnNext(x -> {
//				
//					_log.info(" inserted : " + x.getId() + " -- " + x.getName());
//			}).blockLast();
		
		customerRepo.deleteAll();

		// Way 1 where you use map  
		Flux.fromIterable(testData)
		   .map(x -> {
			   Mono<Customer> newCustomer = customerRepo.save(x);
			   return newCustomer;
		   })
		   .doOnNext(x -> {
			   Customer c = x.block();
			   _log.info("Added : " + c.getId() + " -- " + c.getName());
		   })
		   .blockLast();
//		   .log();
		
		// Way 2
//		Flux.fromIterable(testData)
//			// flatMap take a function that takes a publisher and returns
//			// Object from publisher. .save returns a Mono 
//		   .flatMap(x -> customerRepo.save(x))
//		   .doOnNext(x -> {
//			   _log.info("New Customer : "+ x.getId() + " -- " + x.getName());
//		   })
//		   .log()
//		   .blockLast();

	}

	@Test
	@Order(50)
//	@AfterAll
	public void test_find_all_customers() {

		// @formatter:off

		StepVerifier.create(customerRepo.findAll())
		    .expectSubscription()
		    .expectNextCount(3)
		    .verifyComplete();
		
		 // @formatter:on

	}

	@Test
	public void test_find_by_id() {

		// @formatter:off

		StepVerifier.create(customerRepo.findById("1"))
					.expectSubscription()
					.expectNextMatches(x -> x.getName().equalsIgnoreCase("JPMC"))
					.verifyComplete();
		
		
		 
		// @formatter:on

	}

	@Test
	@Order(3)
	public void test_find_by_address() {

		// @formatter:off
		
		
		StepVerifier.create(customerRepo
				.findByAddress("Mumbai")
				.log("by address - "))
		.expectSubscription()
		.expectNextCount(2)
		.verifyComplete();
		
		// @formatter:on

	}

	@Test
	@Order(1)
	public void test_create_customer() {

		// @formatter:off

		Customer customer = new Customer("101", "BoA", "Bangalore");
		
		Mono<Customer> newCustomer =  customerRepo.save(customer);
		
		StepVerifier.create(newCustomer)
					.expectNextMatches(x -> Objects.nonNull(x.getId()) && x.getName().equals("BoA"))
					.verifyComplete();
		
		StepVerifier.create(customerRepo.findAll())
		     .expectNextCount(4)
		     .verifyComplete();
		   
		// @formatter:on

	}

	@Test
	@Order(2)
	public void test_update_customer() {

		String newAddress = "New York";

		Mono<Customer> jpmcCustomer = customerRepo.findById("1");
		// @formatter:off

		
//		Mono<Customer> customerWithNewAddress = jpmcCustomer
//				   .map(x -> {
//					   x.setAddress(newAddress);
//					   return x;
//				   });
//		
//		Mono<Customer> updatedCustomer = customerRepo.save(customerWithNewAddress.block());
//		
//		
//		StepVerifier.create(updatedCustomer)
//		            .expectNextMatches(x -> x.getAddress().equals("New York"))
//		            .verifyComplete();
		
		
		Mono<Customer> customerWithNewAddress = jpmcCustomer
				   .map(x -> {
					   x.setAddress(newAddress);
					   return x;
				   }).flatMap(x -> {
					   // what you get here is a customer
					   // you need to return a Mono / Flux 
					   // from flatMap [exactly like you have to return a stream from flatMap of stream]
					   // and we anyway wanted to save customer
					   // the repo.save returns a mono. Hence flatMap
					   return customerRepo.save(x);
				   });
		
		StepVerifier.create(customerWithNewAddress)
	        .expectNextMatches(x -> x.getAddress().equals("New York"))
	        .verifyComplete();
			
		
//		updatedCustomer.flatMap(x -> customerRepo.save(x));
		
		// @formatter:on

	}

	@Test
	@Order(51)
	public void test_delete_customer() {

		// @formatter:off

		StepVerifier.create(customerRepo.findById("101")
					.map(Customer::getId)
					.flatMap(x -> {
						// deleteById return Mono<Void>
						return customerRepo.deleteById(x);
					}))
					.expectSubscription()
					.verifyComplete();
	
		// 1 deleted and hence count will be now 3
		StepVerifier.create(customerRepo.findAll())
		            .expectNextCount(3)
		            .verifyComplete();

		 
		// @formatter:on

	}

}
