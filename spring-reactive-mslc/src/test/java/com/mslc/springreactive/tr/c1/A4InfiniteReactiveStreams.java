package com.mslc.springreactive.tr.c1;

import java.time.Duration;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;

public class A4InfiniteReactiveStreams {

	@Test
	public void when_infinite_sequence() {

		// @formatter:off
		
		Flux<Long> values = 
				Flux.interval(Duration.ofSeconds(1))
				.log();
		
		values.subscribe(System.out::println);
		
		// @formatter:on

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Take will take only speicified number of values and then will invoke complete
	 * event
	 * <hR>
	 * Make note of the fact that the 1st test does continue as the thread of test
	 * case is still active
	 * 
	 */
	@Test
	public void when_infinite_sequence_with_take() {

		// @formatter:off

		Flux<Long> values = 
				Flux.interval(Duration.ofSeconds(1))
				.take(3)
			    .log();
		
		values.subscribe(System.out::println);
		 
		// @formatter:on

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * <h2>Assignment</h2> Use a map to convert to Integer Flux and then use
	 * StepVerifier
	 * 
	 */
	@Test
	public void when_infinite_sequence_with_map_take() {

	}

}
