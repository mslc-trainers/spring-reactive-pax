package com.mslc.springreactive.tr.exercise;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.mslc.springreactive.model.Product;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestInstance(Lifecycle.PER_CLASS)
@AutoConfigureWebTestClient
@ActiveProfiles("test")
public class ProductAPIRestControllerTest {

	@Autowired
	ProductRepository productRepo;

	// @formatter:off

	@Autowired
	WebTestClient webTestClient;
	
	// @formatter:on

	@BeforeAll
	public void setUp() {

	
	}

	@Test
	public void testGetProductById() {

		// @formatter:off
		
		Flux<Product> p =	
			webTestClient
			   .get()
			   .uri("/products/{productId}", "1")
			   .accept(MediaType.APPLICATION_JSON)
			   .exchange()
			   .returnResult(Product.class)
			   .getResponseBody();
		
		StepVerifier
		    .create(p)
		    .expectNextCount(1)
		    .verifyComplete();
		
	}

}
