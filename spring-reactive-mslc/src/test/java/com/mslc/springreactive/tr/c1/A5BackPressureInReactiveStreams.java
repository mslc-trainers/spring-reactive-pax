package com.mslc.springreactive.tr.c1;

import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscription;

import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

/**
 * 
 * Take a look at the deck and come back to the test cases here
 * 
 * 
 * @author muhammedshakir
 *
 */
public class A5BackPressureInReactiveStreams {

	/**
	 * 
	 * use thenRequest(1) to request for elements
	 * 
	 * @throws InterruptedException
	 * 
	 */
	@Test
	public void backpressure_on_flux() throws InterruptedException {

		// @formatter:off

		Flux<Integer> intFlux = Flux.range(0, 10)
					.log();
		
	
		StepVerifier.create(intFlux)
				.expectSubscription()
				.thenRequest(1)
				.expectNext(0)
				.thenRequest(5)
				.expectNext(1)
				.expectNextCount(8)
				.expectComplete()
				.verify();
//				.thenCancel()
//				.verify();
		
		

	}

	@Test
	public void backpressure_on_flux_request_subscription() {

		// @formatter:off

			Flux<Integer> intFlux = 
					  Flux
					    .range(0, 15)
					    .log();
			
			
			intFlux.subscribe(x -> {},
					
					System.out::println,
					
					() -> {
						// this will executed only when all elements are requested
						System.out.println("complete");
					},
					
					subscription -> subscription.request(5));
			
			
	 }
	
	
	@Test
	public void backpressure_on_flux_cancel_subscription() {
		
		// @formatter:off
		
		Flux<Integer> intFlux = Flux.range(0, 10).log();
		
		intFlux.subscribe(x -> System.out.print(""),
				System.out::println,
				() -> {
					// this will executed only when all elements are requested
					System.out.println("complete");
				},
				subscription -> {
					
					
					subscription.cancel();
				});
		
		
	}
	
	
	// @formatter:off
 


	/**
	 * As rightly mentioned by Dipanjan, ThreadPoolExecutor is not the solution to your use case. 
	 * At the most one can use ThreadPoolExecutor with a RejectedExecutionHandler
	 * and then perhaps resubmit the task.  Your requirement can be fulfilled with
	 * the Infrastructure where in the Producer is BackPressure aware. 
	 * If you have liberty to use Project Reactor then you can do some thing like
	 * I have demonstrated in following PoC Code
	 */
	
	// @formatter:on
	
	@Test
	public void backpressure_on_flux_custom_subscriber() {
		// @formatter:off
		Flux<Integer> intFlux = Flux.range(0, 15).log();
		
		
		
		intFlux.subscribe(new BaseSubscriber<Integer>() {
			protected void hookOnComplete() {
				System.out.println("Completed.........");
			}
			
			protected void hookOnSubscribe(Subscription subscription) {
				subscription.request(6);
			}
			
			protected void hookOnNext(Integer value) {
					if (value > 0 && value % 5 == 0) {
						System.out.println("Requesting next 5 ...");
						try {
							Thread.sleep(4000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						super.request(5);
						
					}
			}
		});
	}
	 

}
