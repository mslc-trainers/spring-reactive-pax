package com.mslc.springreactive.tr.c1;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class A3HandlingErrorsInReactiveStreams {

	List<String> myClients = Arrays.asList("JPMC", "Nomura", "Morgan Stanley", "BNP Pariba", "BoA");

	private static Logger _log = LoggerFactory.getLogger(A3HandlingErrorsInReactiveStreams.class);

	/**
	 * In the test below, note that Goldman Sachs will be skipped the reason is that
	 * there is an exception before it. Flux processing will jump onto onErrorResume
	 * and shall return the default value
	 * <hr>
	 * Try out the same test case without onErrorResume and make necessary changes
	 * in StepVerifier
	 * 
	 * 
	 */
	@Test
	public void handle_error_using_on_resume() {
		// @formatter:off
		Flux<String> stringFlux = 
				Flux.just("a", "b", "c")
				.concatWith(Flux.error(new RuntimeException("Runtime exception....")))
				.concatWith(Flux.just("Goldman Sachs"))
				.onErrorResume(x -> {
//					_log.info("The error is : " + x.getMessage());
					System.out.println(x.getMessage());
					
					return Flux.just("default value");
				}).log();
		
//		stringFlux.subscribe(System.out::println);
		
		StepVerifier
		     .create(stringFlux)
		     .expectNext("a")
		     .expectNextCount(3)
		     .verifyComplete();
				
		 
		// @formatter:on

	}

	/**
	 * It is almost the same thing. Just use onErrorReturn
	 * 
	 */
	@Test
	public void handle_error_using_on_error_retun() {
		// @formatter:off
		Flux<String> stringFlux = 
				Flux.just("a", "b", "c")
				.concatWith(Flux.error(new RuntimeException("Runtime exception....")))
				.concatWith(Flux.just("Goldman Sachs"))
				.onErrorReturn("onErrorDefault")
				.log();
		
//		stringFlux.subscribe(System.out::println);
		
		StepVerifier
		.create(stringFlux)
		.expectNext("a")
		.expectNextCount(3)
		.verifyComplete();
		
		
		// @formatter:on

	}

	/**
	 * use onErrorMap to convert the exception. Also use expectError in StepVerifier
	 * 
	 */
	

}
