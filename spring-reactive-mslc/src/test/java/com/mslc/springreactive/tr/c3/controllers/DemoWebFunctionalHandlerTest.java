package com.mslc.springreactive.tr.c3.controllers;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureWebTestClient
public class DemoWebFunctionalHandlerTest {

	@Autowired
	WebTestClient webTestClient;

	@Test
	public void when_ints_flux_functional_then_return_integerflux() {

		
		System.out.println("webTestClient : " + webTestClient);
		
		// @formatter:off

			Flux<Integer> ints =	webTestClient.get()
					.uri("/ints-flux-functional")
					.accept(MediaType.APPLICATION_JSON)
					.exchange()
					.expectStatus().isOk()
					.returnResult(Integer.class)
					.getResponseBody();
			
			StepVerifier
			    .create(ints)
			    .expectNext(1)
			    .expectNextCount(4)
			    .verifyComplete();
		
		// @formatter:on

	}
	
	@Test
	public void when_int_mono_functional_then_return_integerMono() {
		
		// Implement this yourself
		
	}

}
