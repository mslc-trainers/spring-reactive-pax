package com.mslc.springreactive.s1;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class A6TestingFluxWithParallelProcessing {

	@Test
	public void flux_buffer() throws InterruptedException {

		// @formatter:off

//		  Flux<Integer> f = 
//				  Flux.range(0, 10)
//				  	.buffer(2)
//				  	.flatMap(x -> Flux.fromIterable(x));
//				  	
//		  f.subscribe(x -> {
//			  System.out.println(x);
//		  });
		  
		Flux<List<Integer>> f0 =  Flux.range(0, 10)
									.buffer(2)
									.log();
		
		f0.subscribe();
		  
		  	
			
	}
	

	@Test
	public void parallel_subcription_demo() throws InterruptedException {
		
		
//		Stream.of(1, 2, 3, 4,5,6,7, 8)
//		   .parallel()
//		   .map(x -> {
//			   
//			   try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			   System.out.println("Thread : " + Thread.currentThread().getName());
//			   return x;
//		   })
//		   .collect(Collectors.toList());
		
		   
	
		
		
		
		
	Flux<Integer> f0 = 	Flux.just(1, 2, 3, 4, 5, 6, 7, 8)
//		    .parallel()
//		    .runOn(Schedulers.parallel())

		    .map(x -> {
		    	
		    	try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//		    	System.out.println(x + " is proccessed in : " + Thread.currentThread().getName());
		    	return x + 1;
		    });
//		    .sequential();
		    
	
	
		f0.subscribeOn(Schedulers.parallel()).subscribe(x -> {
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(x  + " >>  -- " + Thread.currentThread().getName());
			
		});
	
		    
		
		
//		Flux.just(1, 2, 3, 4, 5, 6, 7, 8)
//		    .subscribeOn(Schedulers.parallel())
//		    .map(x ->  {
//		    	try {
//					Thread.sleep(1000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//		    	System.out.println(x + " is processed in : " + Thread.currentThread().getName());
//		    	return x;
//		    })
//		    .subscribe();
		
		Thread.sleep(10000);

	}
	

	@Test
	public void parallel_subscription() {

		int min = 0;
		int max = 100;

		// @formatter:off
		
//	 Integer lastInt =	 Flux.range(min, max)
//		   .buffer(10)
//		   .log()
//		   .flatMap(x -> Flux.fromIterable(x))
//		   .blockLast();
//		
//
//		System.out.println(lastInt);
//
//		List<Integer> allInts1 = 
//				Flux.range(min, max)
//				.buffer(10)
//				.log()
//				.flatMap(x -> Flux.fromIterable(x))
//				.collectList()
//				.block();
//
//		System.out.println(allInts1);
//		System.out.println(" ****** ");
		
		

		long start = System.currentTimeMillis();
		
		List<Integer> allInts2 = 
				Flux.range(0, 10)
				.buffer(1)
				.flatMap(x -> {
					
//						System.out.println("This will be main Thread : " + Thread.currentThread().getName());
					
						return	Flux.fromIterable(x)
							  .subscribeOn(Schedulers.parallel())
							  .map(y -> {
								  
								  try {
									Thread.sleep(1000);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								  return y + 1;
							  })
							  .log();
							
//					return Flux.fromIterable(x);
				})
				.collectList()
				.block();

		// @formatter:on

		System.out.println(allInts2);
		long end = System.currentTimeMillis();
		System.out.println((end - start));

	}

	private List<Integer> concat3Values(Integer i) {
		return Arrays.asList(i + 1, i + 2, i + 3);
	}

	@Test
	public void parallel_demo() throws InterruptedException {

		long s = System.currentTimeMillis();
		Flux<Integer> i = Flux.just(1, 2, 3, 4, 5).parallel(1).runOn(Schedulers.parallel()).map(x -> {
			return x;
		})
//				  System.out.println("before subscribeOn - anywhere in chain - does not matter"+ Thread.currentThread().getName());

//			  .subscribeOn(Schedulers.parallel())
				.map(x -> {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(Thread.currentThread().getName());
					return x + 1;
				}).sequential();

		i.subscribe(System.out::println, null, () -> {
			long e = System.currentTimeMillis();
			System.out.println("Total Time take : " + (e - s));
		});

		Thread.sleep(10000);

	}

}
