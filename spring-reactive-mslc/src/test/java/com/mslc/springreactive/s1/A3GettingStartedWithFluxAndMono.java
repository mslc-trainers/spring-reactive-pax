package com.mslc.springreactive.s1;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class A3GettingStartedWithFluxAndMono {

	
	@Test
	public void with_mono_and_flux() throws InterruptedException {
		
		Mono<Integer> m1 = Mono.just(1);
		m1.subscribe(x -> {
			System.out.println(x);
		});
		
		Flux<Integer> f1 = Flux.just(1, 2, 3);
		
		Flux.fromIterable(Arrays.asList(1, 2, 3, 4));
		
		Flux<Integer> f3 = Flux.fromArray(new Integer[] {1, 2, 3});
		Flux<Integer> f4 =  Flux.fromStream(Stream.of(1, 2, 4));
		Flux<Integer> f5 = Flux.from(m1);
		Flux<Integer> f6 =  Flux.range(0, 5);
		
		
		
		f1.subscribe(
				
						x -> {System.out.println("received : " + x);}, 
						
						x -> {System.out.println(x.getMessage());},
						
						() -> {
							System.out.println("onComplete.....");
						}
				);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
//		
//		
//		
//		f1.subscribe(x -> {
//			System.out.println(x + " -- " + Thread.currentThread().getName());
////			throw new RuntimeException("error");
//		}, x -> {
//			System.out.println("error encountered: " + x.getMessage());
//		}, () ->  {
//			System.out.println("Completed : " + Thread.currentThread().getName());
//		});
		
//		Flux<Long> intervalFlux = 
//				Flux.interval(Duration.ofMillis(3000), Duration.ofMillis(1000));
//		
//		intervalFlux.subscribe(x -> {
//			
//			Thread t = Thread.currentThread();
//			System.out.println(x + " -- " + t.getName() + " -- " + t.getThreadGroup().getName() + " -- " + t.isDaemon());
//		});
//		
//		Thread.sleep(Long.MAX_VALUE);
		
	}
	
	
	@Test
	public void should_combine_two_flux_using_contact() {
		
		int min = 0;
		int max = 10;
		// @formatter:off

		Flux<Integer> evenNumbers = 
				Flux.range(min, max)
						.filter(x -> x % 2  == 0);				 
		
		Flux<Integer> oddNumbers = 
				Flux.range(0, 10)
				     .filter(x -> x % 2 > 0);
				
		Flux<Integer> fluxOfIntegers = Flux.concat(evenNumbers, oddNumbers);
		

		List<Integer> result = 
				 fluxOfIntegers
				 .collectList()
				 .block();
		 

		Mono<List<Integer>> monoResult = 
				  fluxOfIntegers
				    .collectList();
		
		result = monoResult.block();
		    
		assertThat(result.size()).isEqualTo(max);
		
		
	}

}
