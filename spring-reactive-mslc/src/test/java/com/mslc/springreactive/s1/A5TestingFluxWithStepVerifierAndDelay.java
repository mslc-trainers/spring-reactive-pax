package com.mslc.springreactive.s1;

import java.time.Duration;
import java.util.List;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import reactor.util.function.Tuple2;

public class A5TestingFluxWithStepVerifierAndDelay {

	@Test
	public void understanding_merge() throws InterruptedException {

		// @formatter:off
		Flux<String> f1 = Flux.just("f1-1", "f1-2", "f1-3");

		f1  = 
				 Flux
				    .interval(Duration.ofMillis(1000))
				    .zipWith(f1, (x, y) -> y);

		
		Flux<String> f2 = Flux.just("f2-1", "f2-2")
						.doOnNext(x -> {
						
							try {
								Thread.sleep(2000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						});
		Flux<String> f3 = Flux.just("f3");
		
		// the doc says that subscriber is attached eagerly to each one of them and will 
		// provide interleaved results. 
//		Flux.merge(f1,f2,f3)
//		    .log()
//		    .subscribe(System.out::println);
		
//		System.out.println(" ---------- ");
//		// the doc says that subscribers are attached to the 1st source 
//		// on complete of first source, it subscribes to second source
//		// provide interleaved results. 
		Flux.concat(f1,f2,f3)
		  .log()
		  .subscribe(System.out::println);
				

		System.out.println("Putting main thread to sleep....");
		Thread.sleep(10000);
	}

	@Test
	public void tuple_demo() throws InterruptedException {

		Flux<Long> f1 = Flux.just(1L, 2L, 3L);

		Flux<Tuple2<Long, Long>> f2 = Flux.interval(Duration.ofMillis(1000)).zipWith(f1);
		f2.subscribe(x -> {
			System.out.println(x.getT1() + " -- " + x.getT2());
		});

		Thread.sleep(10000);

	}

	@Test
	public void when_mergedWithDelayedElements() throws InterruptedException {

		int min = 0;
		int max = 6;

		// @formatter:off
 
		// 0, 2, 4
		Flux<Integer> evenNumbers =
				 Flux.range(min, max)
				 .filter(x -> x % 2 == 0);
		
		
		// 1, 3, 5
		Flux<Integer> oddNumbers = 
				 Flux.range(min, max)
				 .filter(x -> x % 2 > 0);
//		oddNumbers.subscribe(System.out::println);
		System.out.println(" -- ");

		/**
		 * there can be one service that may take more time than the other
		 * . you may introduce delay in order to make sure that your processing 
		 * does not depend on timing of each one of those services
		 * 
		 * Flux must be time agnostic
		 */
		
			
		
		Flux<Integer> mergedFluxes = 
				 Flux.merge(
						 evenNumbers.delayElements(Duration.ofMillis(1000L)), 
						 oddNumbers.delayElements(Duration.ofMillis(500L)));
		
//		mergedFluxes.subscribe(System.out::println);
	
		StepVerifier.create(mergedFluxes.log())
		   .expectNext(1)
		   .expectNext(0)
		   .expectNext(3)
		   .expectNext(5)
		   .expectNext(2)
		   .expectNext(4)
		   .expectComplete()
		   .verify();
//		
//		Mono<List<Integer>> allValues =   mergedFluxes.collectList();
//		
//		allValues.block().forEach(System.out::println);
		
		// @formatter:on
		

	}

}
