package com.mslc.springreactive.s1;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;

public class A2SolvingIssueWithCompletableFutureUsingFlux {

	@Test
	public void using_future() throws InterruptedException, ExecutionException {

		ExecutorService service = Executors.newFixedThreadPool(10);

		Future<Integer> f0 = service.submit(new Callable<Integer>() {

			@Override
			public Integer call() throws Exception {
				Thread.sleep(5000);
				return 1;
			}
		});

		Integer i1 = f0.get();

		Future<Integer> f1 = service.submit(new Callable<Integer>() {

			@Override
			public Integer call() throws Exception {
				// TODO Auto-generated method stub
				return i1 + 10;
			}

		});

		Integer i2 = f1.get();

	}

	/**
	 * This test illustrates the difference between Async v/s Sync. The sync methods
	 * will execute in the containing threads where as async will execute in default
	 * or supplied Executor
	 * 
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void understanding_async() throws InterruptedException {
		CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> 0);

		// @formatter:off

		future.thenApply( x -> {
			System.out.println("NonAysnc 1 " + Thread.currentThread().getName());

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return x + 1 ;
		})
	      .thenApply( x -> {
	    	  System.out.println("NonAsycn 2 " + Thread.currentThread().getName());
	    	  return x + 1;
	      })
	      .thenAccept( x -> System.out.println(x));
		
		System.out.println("after my computation..");
		
		future.thenApplyAsync( x -> {
			System.out.println("Aysnc 1 " + Thread.currentThread().getName() + " -- " + Thread.currentThread().isDaemon());
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return x + 1;
		})   // first step
	      .thenApplyAsync( x -> {
	    	  System.out.println("Async 2 " + Thread.currentThread().getName());
	    	  return x + 1;
	      })   // second step
	      .thenAccept( x -> System.out.println(x)); 
		
		System.out.println("After registering the handlers..");
		Thread.sleep(7000);

	}

	@Test
	public void using_completablefuture() throws InterruptedException, ExecutionException {

		// @formatter:off
		
		long start = System.currentTimeMillis();
 
		CompletableFuture<Integer> serviceCall1 = CompletableFuture.supplyAsync(() -> {

			System.out.println("serviceCall1 : " + Thread.currentThread().getName());
			
			return 1;
		});

	   CompletableFuture<Integer> serviceCall2 = 
		    serviceCall1.handle((x, y) -> {
		    	System.out.println("serviceCall2 : " + Thread.currentThread().getName());
		    	try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    	
				if (y == null) {
					return x + 10;
				} else {
					// handle error;
					y.printStackTrace();
					return -1;
				}
			});
	   
	   CompletableFuture<Integer> serviceCall3 = 
			   serviceCall2.handle((x, y) -> {
				   System.out.println("serviceCall3 : " + Thread.currentThread().getName());
				  // not handling error for sake of simplicty
				   
				   try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				   return x + 15;
			   });
	   
	   System.out.println("handlers registered....");

	   Integer i = serviceCall3.get();
	   long end = System.currentTimeMillis();	   
	   System.out.println(i + " in " + (end-start) + " milli-seconds");
	   
	   // now imagine thaty you have to process not one but several such values in a loop
	   // and accumulate the value in a list
	   
	   
	   
	   

	}

	@Test
	public void using_reactive_api_instead_of_completablefuture() throws InterruptedException {

		// assertThat(component)
		// @formatter:off

		    long start = System.currentTimeMillis();
		    
		    List<Integer> res0 = 
		    		Flux.just(1)
		    		.map(x -> {
		    			// execute service 1
		    			try {
							Thread.sleep(5000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		    			return x + 10;
		    		})
		    		.doOnError(System.out::println)
		    		.map(x -> {
		    			try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		    			// execute service 2
		    			return x + 15;
		    		}).collectList()
		    		.block();
		    long end  = System.currentTimeMillis();
		    System.out.println(res0 + " -- " + (end - start));
		    
		    if (true) {
		    	return ;
		    }
		    		
		    long start2 = System.currentTimeMillis();
			List<Integer> res =
					// obtaining the value from service 1
					Flux.range(0, 3)
					// each value is consumed service 2
				   .map(v -> action2(v))
				   // handle error
				   .doOnError(System.out::println)
				   // each value returned by service 2 (which is a supplier)
				   // is further processed by another map
				   .map(this::executeAsynCall)
				   // finally, all the results are collected in a list
				   .collectList()
				   .block();
			
		// @formatter:on
		long end2 = System.currentTimeMillis();
		System.out.println("time taken : " + (end2 - start2));

		System.out.println(res.size() + " -- " + res);
		Thread.sleep(7000);

	}

	public void fallBackAction() {

	}

	private Integer executeAsynCall(Supplier<Integer> v) {
		System.out.println("Task 2 started");
//		try {
//			Thread.sleep(3000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return 2 + v.get();
	}

	public Supplier<Integer> action2(Integer value) {

		System.out.println("Task 1 started...");
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return () -> 1 * 100;
	}

}
