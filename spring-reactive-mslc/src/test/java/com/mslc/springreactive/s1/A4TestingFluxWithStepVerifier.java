package com.mslc.springreactive.s1;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class A4TestingFluxWithStepVerifier {

	@Test
	public void should_combine_two_flux_using_contact() {

		int min = 0;
		int max = 6;
		// @formatter:off

		Flux<Integer> evenNumbers = 
				Flux.range(min, max)
						.filter(x -> x % 2  == 0);				 
		
		Flux<Integer> oddNumbers = 
				Flux.range(min, max)
				     .filter(x -> x % 2 > 0);
				
		Flux<Integer> fluxOfIntegers = 
					Flux.concat(evenNumbers, oddNumbers, Mono.just(1))
					.log();
					
		
//		Duration d = StepVerifier.create(fluxOfIntegers)
//		    .expectNext(0)
//		    .expectNext(2)
//		    .expectNext(4)
//		    .expectNext(1)
//		    .expectNext(3)
//		    .expectNext(5)
//		    // expectComplete when last element is emitted
//		    .expectComplete()
//		    .verify();
//		// @formatter:on

//		System.out.println(d.get(ChronoUnit.NANOS));
		
		 StepVerifier.create(fluxOfIntegers)
			    .expectNext(0)
			    .expectNext(2)
			    .expectNext(4)
			    .expectNext(1)
			    .expectNext(3)
			    .expectNext(5)
			    .expectNext(1)			    
			    .verifyComplete();
		 
			  
			
			

	}

}
