package com.mslc.springreactive.s1;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

public class A1UsingCompletableFuture {

	@Test
	public void register_handler_to_execute_on_completion_of_task() throws InterruptedException {

		CompletableFuture<String> cf = CompletableFuture.supplyAsync(() -> {

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "IBM";
		});

		System.out.println("Registering the handler to executed when ever the task is completed");
		cf.whenComplete((x, y) -> {
			if (y == null) { // no exception
				System.out.println("Handler executed. Value received from the task : " + x);
			}
		});
		System.out.println("Handler Registered...");
		Thread.sleep(5000);

	}

	@Test
	public void the_whencomplete_pyramid() throws InterruptedException {
		
		long start = System.currentTimeMillis();

		CompletionStage<Integer> stage1 = CompletableFuture.supplyAsync(() -> {
			// Call to external service 1 that returns a value
			// Assume that this service takes 3 seconds
			try {
				System.out.println("stage1 task executing in : " + Thread.currentThread().getName());
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 5;
		});

		stage1.whenComplete((x, y) -> {

			CompletionStage<Integer> stage2 = CompletableFuture.supplyAsync(() -> {

				// Call external service 2 with the value received from service 1 which is x
				// y is the exception if at all thrown by stage1 task
				// Assume that this service takes 4 seconds

				try {
					System.out.println("stage2 task executing in : " + Thread.currentThread().getName());
					Thread.sleep(4000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return x + 10;
			});

			stage2.whenComplete((a, b) -> {
				// Again may be a call to some external service.

				CompletionStage<Integer> stage3 = CompletableFuture.supplyAsync(() -> {

					try {
						System.out.println("stage3 task executing in : " + Thread.currentThread().getName());
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return a + 20;
				});
				
				stage3.whenComplete((finalValue, exception) -> {
					long end = System.currentTimeMillis();
					System.out.println("finalValue : " + finalValue + " -- " + (end - start));
				});
			});
			
		
			
		});
		
		System.out.println("All services fired....");
		
		Thread.sleep(15000);

	}
	
	@Test
	public void the_whencomplete() throws InterruptedException {
		
		long start = System.currentTimeMillis();
		
		CompletableFuture<Integer> stage1 = CompletableFuture.supplyAsync(() -> {
			// Call to external service 1 that returns a value
			// Assume that this service takes 3 seconds
			try {
				System.out.println("stage1 task executing in : " + Thread.currentThread().getName());
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 5;
		});
		
		CompletableFuture<Integer> stage2 = stage1.handle((x, y) -> {
			System.out.println("Stage2 executing in : " + Thread.currentThread().getName());

			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return x + 10;
		});
		
		CompletableFuture<Integer> stage3 =  stage2.handle((x, y) -> {
			System.out.println("Stage3 executing in : " + Thread.currentThread().getName());

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			return x + 20;
		});
		
		stage3.whenComplete((x, y) -> {
			long end = System.currentTimeMillis();
			System.out.println("final value: " + x + " time taken : " + (end - start));
		});
		
		
		
		
		System.out.println("All services fired....");
		
		Thread.sleep(15000);
		
	}

	@Test
	public void would_result_in_maintenance_hell() throws InterruptedException, ExecutionException {

		// @formatter:off
		
	 
		 long start = System.currentTimeMillis();
		 
	
		  CompletableFuture.supplyAsync(action())
		  .whenComplete((x, y) -> {
			 if (y != null) {
				 fallBackAction(y);
			 } else {
				 CompletableFuture<String> f1 = CompletableFuture.supplyAsync(() -> {
					 
					 System.out.println("Starting action 2");
					 
					 try {
						Thread.sleep(3000);
					 } catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					 }
					 return   "action 2";
				 });
				 f1.whenComplete((a, b) -> {
					 
					 /**
					  * 
					  * Some processing here. May be a call to external service
					  */
			
					 System.out.println("completed...");
					 long end = System.currentTimeMillis();
					 System.out.println("time taken : " + (end-start));
					 System.out.println(" :" + (x + " + " + a));
					 
				 });
			 }
		  });

	   
		Thread.sleep(7000);
		

	}

	public void fallBackAction(Throwable t) {

		System.out.println("The task returned an error : " + t.getMessage());

	}

	public Supplier<String> action() {

		System.out.println("action 1 started...");
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return () -> "action 1";
	}

}
