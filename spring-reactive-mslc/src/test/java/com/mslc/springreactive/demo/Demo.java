package com.mslc.springreactive.demo;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import com.mslc.springreactive.model.Invoice;
import com.mslc.springreactive.model.Product;
import com.mslc.springreactive.util.DataBuilder;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class Demo {

	@Test
	public void flat_map_with_streams() {

		List<Invoice> invoices = DataBuilder.buildInvoices();
		// @formatter:off

		List<Product> allProducts =
			invoices
			  .stream()
			  .flatMap(x -> 
//			  			x.getProducts().stream()
			  			Stream.of(x.getProducts().toArray(new Product[] {})))
			  .collect(Collectors.toList());

		allProducts.forEach(System.out::println);
		 
		// @formatter:on

	}
	
	

	@Test
	public void flat_map_with_webflux() {

		// @formatter:off
		
		Mono<String> m = Mono.just("a");
		
		Mono<Integer> m2 = m.map(x ->  x.length());
		Flux<String> m5 = Flux.just("a", "b");

		Flux<Integer> m3 = m5
						.flatMap(x -> 
							Flux.just(x.length(), 3));
		
		
//		m2.subscribe(System.out::println);
		m3.subscribe(System.out::println);
		System.out.println(" ----- ");

		Stream<Integer> s = 
				Stream.of("a", "b")
				.flatMap(x -> Stream.of(x.length(), 1));

		List<Integer> l = s.collect(Collectors.toList());
		System.out.println(l);

		Flux<String> f = Flux.just("a", "b", "c");

		f.flatMap(x -> Flux.just(1, x.length() + 1)).subscribe(System.out::println);

		// @formatter:on

//		f.subscribe(System.out::println);

	}

}
