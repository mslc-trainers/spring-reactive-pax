package com.mslc.springreactive.gettingstarted.completablefuture;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.junit.jupiter.api.Test;

public class UnderstandingCompletableFuture {
	
	
	@Test
	public void test_simple_completed_completablefuture() {
		
	
		
		CompletableFuture<String> cf = CompletableFuture.completedFuture("completed");
		
		assertTrue(cf.isDone());
		try {
			assertEquals("completed", cf.get());
		} catch (InterruptedException | ExecutionException e) {
			fail("No Exception Expected");
			
		}
		
		
	}

}
