package com.mslc.springreactive.s3;

import org.junit.jupiter.api.Test;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.mslc.springreactive.routers.TrainingRouter;

public class TrainingRouterTest {

	@Test
	public void when_all_trainings_return_trainings() {

		// @formatter:off
		WebTestClient testClient = 
				WebTestClient
				.bindToRouterFunction(new TrainingRouter().allTrainings())
				.build();
		 
		
		testClient.get().uri("/trainings")
		   .exchange()
		   .expectStatus().isOk()
		   .expectBody()
		   .jsonPath("$").isNotEmpty()
		   .jsonPath("$.trainingId").isEqualTo("0");
		
		
		   
		   
		   
		
		
	

	   


	}

}
